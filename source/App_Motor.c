#include "App_Motor.h"
#include "MYinit.h"

MOTOR_HANDLER_t my_motor_handler;

void Motor_Control(void* parameter)
{
	rt_uint32_t level;		
	
	Motor_para_init(&my_motor_handler);//程序参数初始化
	Motor_brake();
	rt_thread_mdelay(500);
	
  while (1)
  {	
		//初始化相关
		
		level = rt_hw_interrupt_disable();//临时关闭中断，防止数据同时修改		
		switch(my_motor_handler.current_step)
		{
			case 	M_Step_Opened:		M_step_opened_fun		(&my_motor_handler);break;
			case 	M_Step_Opening:		M_step_opening_fun	(&my_motor_handler);break;
			case 	M_Step_Brake:			M_step_brake_fun		(&my_motor_handler);break;
			case 	M_Step_Closing:		M_step_closing_fun	(&my_motor_handler);break;
			case 	M_Step_Closed:		M_step_closd_fun		(&my_motor_handler);break;
			default:
				break;	
		}		
		rt_hw_interrupt_enable(level);	//打开中断，可以继续使用。
		
		if(my_motor_handler.motor_sleep_counter<0xffffff00)//防止溢出，理论上连续大约13年以上会溢出
			my_motor_handler.motor_sleep_counter++;//电机睡眠状态计时累积		
		if(my_motor_handler.cycle_counter<0xffffff00)//防止溢出，理论上连续大约13年以上会溢出
			my_motor_handler.cycle_counter ++;//更新程序循环计数器
		
		rt_thread_mdelay(MOTOR_CYCLE_TIME_MIN);	//循环延时	

	}//while1结束
	
}


void Motor_open(void)				//打开方向
{
	Gpio_SetIO(GpioPortA, GpioPin7);
	Gpio_ClrIO(GpioPortB, GpioPin0);	
}
//
void Motor_close(void)				//关闭方向
{
	Gpio_SetIO(GpioPortB, GpioPin0);
	Gpio_ClrIO(GpioPortA, GpioPin7);			
}
//
void Motor_brake(void)				//刹死
{
	
	Gpio_SetIO(GpioPortA, GpioPin7);
	Gpio_SetIO(GpioPortB, GpioPin0);
}
//
void Motor_free(void)				//松开
{
	Gpio_ClrIO(GpioPortA, GpioPin7);	
	Gpio_ClrIO(GpioPortB, GpioPin0);		
}
//
int Intent_add(MOTOR_HANDLER_t *motor_handler,ENUM_VOLVE_STATUS_t intent,ENUM_Motro_Ctr_Source_t	source)//追加一个新动作位置
{
	rt_uint32_t level;	
	
	int warning=0;
	
	level = rt_hw_interrupt_disable();//临时关闭中断，防止数据同时修改		
	
	motor_handler->intent_list[motor_handler->list_empty].intent	= intent;
	motor_handler->intent_list[motor_handler->list_empty].source 	=	source;
	motor_handler->list_empty=get_next_list(motor_handler->list_empty,LEN_INTENT_LIST);	
	if(motor_handler->list_empty == motor_handler->list_last)
	{
		motor_handler->list_last=get_next_list(motor_handler->list_last,LEN_INTENT_LIST);	
		warning=-1;
	}
	
	rt_hw_interrupt_enable(level);	//打开中断，可以继续使用。
	
	return warning;//0正常，-1 有未用的位置被覆盖，会造成丢失位置（这是追求最终位置优先的方式，如果不允许位置丢失，要先检查，异常禁止插入）

}
//
void Intent_set(MOTOR_HANDLER_t *motor_handler,ENUM_VOLVE_STATUS_t intent,ENUM_Motro_Ctr_Source_t	source)//立即执行新动作位置
{
	rt_uint32_t level;	
	
	level = rt_hw_interrupt_disable();//临时关闭中断，防止数据同时修改		
	
	motor_handler->current_intent.intent = intent;
	motor_handler->current_intent.source = source;	
	motor_handler->list_last = motor_handler->list_empty;
	
	rt_hw_interrupt_enable(level);	//打开中断，可以继续使用。	
}
//
int  Intent_next(MOTOR_HANDLER_t *motor_handler)//切换下一个动作命令 返回0，切换成功        返回-1，动作已执行完毕，列表已空
{
	int temp=-1;
	rt_uint32_t level;	
	
	level = rt_hw_interrupt_disable();//临时关闭中断，防止数据同时修改		
		
	if(motor_handler->list_empty != motor_handler->list_last)
	{
		motor_handler->current_intent.intent		=		motor_handler->intent_list[motor_handler->list_last].intent;
		motor_handler->current_intent.source 		=		motor_handler->intent_list[motor_handler->list_last].source;
		motor_handler->list_last=get_next_list(motor_handler->list_last,LEN_INTENT_LIST);	
		temp=0;
	}
	rt_hw_interrupt_enable(level);	//打开中断，可以继续使用。

	return temp;
}
//
void M_step_brake_fun		(MOTOR_HANDLER_t *motor_handler)
{
	int intent_valid;
	
	if(motor_handler->cycle_counter < m_time_fac*0.25)//刹车前松抱闸延时
	{		
		Motor_free(); 
	}
	else
	{		
		Motor_brake(); 
		intent_valid=FALSE;		
		if(motor_handler->valve_status != ValveERROR)
		{
			Intent_next(motor_handler);//刷新current intent目标位置
			
			if(motor_handler->current_intent.intent != motor_handler->valve_status)//如果目标状态有变化，则执行跳转
			{
				switch(motor_handler->current_intent.intent)
				{
					case ValveOPENED:
						motor_handler->current_step=M_Step_Opening;intent_valid=TRUE;break;//跳转到打开阀状态
					case ValveCLOSED:
						motor_handler->current_step=M_Step_Closing;intent_valid=TRUE;break;//跳转到关闭阀状态
					default ://异常状态
						intent_valid=FALSE;break;
				}	
				
				if(intent_valid == TRUE)//如果条状目标有效，则执行一些扫尾工作
				{
					motor_handler->valve_status= ValveMIDDLE;//删除当前到位状态，标记为位置不定状态
					motor_handler->cycle_counter=0;//重置程序循环计数器
					motor_handler->prev_step = M_Step_Brake;//保存上一个步序记录
				}
				
			}//if motor_intent_changed
			
		}//if err==0
	
	}
	
}//brake_func
//
void M_step_opening_fun	(MOTOR_HANDLER_t *motor_handler)
{
	
	static unsigned int step_over=FALSE;

	Motor_open();			//打开调试，临时关闭
	
	if(Motor_sensor_opened() == 1)
		motor_handler->sensor_counter++;
	else
		motor_handler->sensor_counter = 0;
	
	if(motor_handler->sensor_counter > 1)//连续100ms检测到传感器信号，则认为运行到位
	{
		motor_handler->current_step = M_Step_Opened;
		step_over = TRUE;
	}
	if(motor_handler->cycle_counter > 60 * m_time_fac )//连续60秒未执行到位，则认为运行错误,转到刹车状态
	{
		motor_handler->err_str = STR_MOTOR_ERR_TIME_OUT;
		motor_handler->valve_status = ValveERROR;
		motor_handler->current_step = M_Step_Brake;
		step_over = TRUE;
	}
	
	if(motor_handler->current_intent.intent != ValveOPENED)//检测到方向发生变化，转到刹车停止步序
	{
		motor_handler->current_step=M_Step_Brake;
		step_over = TRUE;
	}
	
	if(step_over == TRUE)
	{
		motor_handler->prev_step = motor_handler->current_step;//保存上一个步序记录
		motor_handler->sensor_counter=0;	
		motor_handler->cycle_counter = 0;
		step_over = FALSE;
	}
	
	
	

}
//
void M_step_opened_fun	(MOTOR_HANDLER_t *motor_handler)
{
	motor_handler->prev_step = motor_handler->current_step;//保存上一个步序记录
	motor_handler->valve_status=ValveOPENED;
	motor_handler->cycle_counter = 0;
	motor_handler->current_step=M_Step_Brake;
	if(motor_handler->last_sensor_pos == 0)
	{
		motor_handler->last_sensor_pos = 1;//打开到位传感器触发，标记1
		motor_handler->motor_sleep_counter=0;//清除睡眠计时
	}
}
//
	
void M_step_closing_fun	(MOTOR_HANDLER_t *motor_handler)
{
	
	static unsigned int step_over=FALSE;

	Motor_close();			//打开调试，临时关闭
	
	if(Motor_sensor_closed() == 1)
		motor_handler->sensor_counter++;
	else
		motor_handler->sensor_counter = 0;
	
	if(motor_handler->sensor_counter > 1)//连续100ms检测到传感器信号，则认为运行到位
	{
		motor_handler->current_step = M_Step_Closed;
		step_over = TRUE;
	}
	if(motor_handler->cycle_counter > 60 * m_time_fac )//连续60秒未执行到位，则认为运行错误,转到刹车状态
	{
		motor_handler->err_str = STR_MOTOR_ERR_TIME_OUT;
		motor_handler->valve_status = ValveERROR;
		motor_handler->current_step = M_Step_Brake;
		step_over = TRUE;
	}
	
	if(motor_handler->current_intent.intent != ValveCLOSED)//检测到方向发生变化，转到刹车停止步序
	{
		motor_handler->current_step=M_Step_Brake;
		step_over = TRUE;
	}
	
	if(step_over == TRUE)
	{
		motor_handler->prev_step = motor_handler->current_step;//保存上一个步序记录
		motor_handler->sensor_counter=0;	
		motor_handler->cycle_counter = 0;
		step_over = FALSE;
	}
	
	

}
//
	
void M_step_closd_fun		(MOTOR_HANDLER_t *motor_handler)
{
	motor_handler->prev_step = motor_handler->current_step;//保存上一个步序记录
	motor_handler->valve_status=ValveCLOSED;
	motor_handler->cycle_counter = 0;
	motor_handler->current_step=M_Step_Brake;
	motor_handler->motor_sleep_counter=0;
	if(motor_handler->last_sensor_pos == 1)
	{
		motor_handler->last_sensor_pos = 0;//关闭到位传感器触发，标记0
		motor_handler->motor_sleep_counter=0;//清除睡眠计时
	}
}
	//
void Motor_err_rst		(MOTOR_HANDLER_t *motor_handler)//运行故障复位
{	
	rt_uint32_t level;	
	level = rt_hw_interrupt_disable();//临时关闭中断，防止数据同时修改	
	motor_handler->err_str = 0;	
	if(motor_handler->valve_status == ValveERROR)//判断，防止不必要的调用，破坏正常状态
		motor_handler->valve_status= ValveMIDDLE;
	rt_hw_interrupt_enable(level);	//打开中断，可以继续使用。	
}
//

void Motor_para_init		(MOTOR_HANDLER_t *motor_handler)//程序参数初始化
{	
	
	
	if(Motor_sensor_opened())//防止上电向限位方向移动，连续的上电移动会卡滞齿轮箱，初始化为默认运动一次
	{
		motor_handler->valve_status = ValveOPENED;
		motor_handler->current_intent.intent=ValveOPENED;		//2021年4月14日默认开阀，根据霍工的意见，意外断电最好保持原状
		
	}
	if(Motor_sensor_closed())
	{
		motor_handler->valve_status = ValveCLOSED;
		motor_handler->current_intent.intent=ValveCLOSED;		//2021年4月14日默认开阀，根据霍工的意见，意外断电最好保持原状
	}
	
	
	if( (!Motor_sensor_closed()) && (!Motor_sensor_opened()))
	{
		motor_handler->valve_status = ValveMIDDLE;
		motor_handler->current_intent.intent=ValveCLOSED;		//2021年4月14日默认开阀，根据霍工的意见，意外断电，保持关阀
	}
	
	motor_handler->current_intent.source=Source_Program_Routine;	
	motor_handler->current_step = M_Step_Brake;
	motor_handler->prev_step = M_Step_Brake;	
	
	motor_handler->cycle_counter = 0;	
	motor_handler->err_str = 0;	
	motor_handler->intent_list[0].intent=ValveOPENED;	//不用也行
	motor_handler->intent_list[0].source=Source_Program_Routine;	//不用也行	
	motor_handler->list_empty = 0;	
	motor_handler->list_last = 0;	
	motor_handler->sensor_counter = 0;	
}


void Motor_reset_sleep_counter(MOTOR_HANDLER_t *motor_handler)//程序参数初始化
{	
	rt_base_t level;
	level = rt_hw_interrupt_disable();//临时关闭中断，防止数据同时修改		
	motor_handler->motor_sleep_counter = 0;	
	rt_hw_interrupt_enable(level);	//打开中断，可以继续使用。
}


unsigned int get_next_list(unsigned int current,unsigned int len_of_list)//获取列表的下一个位置（到最大之后，返回最初）
{
	if(current == len_of_list)
		return 0;
	else	
		return current+1; 
}


int  Motor_sensor_opened(void)	//查询打开到位传感器状态0：未到位；1：到位；-1：错误
{	
	return !Gpio_GetInputIO(GpioPortA, GpioPin10);		
}
//
int  Motor_sensor_closed(void)	//查询关闭到位传感器状态
{	
	return !Gpio_GetInputIO(GpioPortB, GpioPin12);		
}
//

unsigned int motor_cycles_to_ms(unsigned int cycles)//将电机循环的运行周期数转换为时间ms
{
	return cycles * MOTOR_CYCLE_TIME_MIN;	
}
//

unsigned int motor_cycles_to_s(unsigned int cycles)//将电机循环的运行周期数转换为时间s
{
	long long temp;
	temp = cycles  * MOTOR_CYCLE_TIME_MIN;
	return (unsigned int ) (temp / 1000 ); 
}


unsigned int motor_ms_to_cycles(unsigned int time_ms)//将预期时间转换为电机循环的运行周期数
{
	return (time_ms < MOTOR_CYCLE_TIME_MIN) 	? 	( 1 )		: 	(time_ms / MOTOR_CYCLE_TIME_MIN);
}

unsigned int motor_s_to_cycles(unsigned int time_s)//将预期时间转换为电机循环的运行周期数
{
	long long temp;
	temp = time_s  * 1000;
	return (temp < MOTOR_CYCLE_TIME_MIN) 	? 	( 1 )		: 	(temp / MOTOR_CYCLE_TIME_MIN);
}

//





