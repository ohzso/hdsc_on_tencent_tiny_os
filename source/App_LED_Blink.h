#ifndef  _APP_LED_BLINK
#define  _APP_LED_BLINK

#include "App_Client.h"

#include <rtthread.h>

void App_LED_Blink(void* parameter);

#define LED_Blink_STACK_SIZE  128
#define LED_Blink_PRIORITY     80
#define LED_Blink_TIMESLICE   100


#define LED_Blink_CYCLE   10			//循环最小时间 ms



#define BEEP_OFF		Gpio_ClrIO(GpioPortA, GpioPin11);
#define BEEP_ON	Gpio_SetIO(GpioPortA, GpioPin11);


#define LED_BUF_LEN 10				//建立一个buffer，每个元素的每个bit代表一个LED，不同元素所有bit进行按位或运算，将最终结果显示再LED上
#define LED_CLIENT  0					//wifi client的led指示
#define LED_SLEEP_SERVER  1		//wifi client的led指示
#define LED_USER_KEY			2   //user的按键操作led指示
#define LED_TEMP					3		//临时的一些杂项显示辅助



void LED_Control(unsigned int led_remap);//最终进行显示
void LED_loop(unsigned int *led_buf,unsigned char buffer_len);//对所有buffer内容进行按位或运算，并最终显示在LED上

unsigned int client_leds(void);//wifi client的led状态
unsigned int sleep_server_leds(void);//sleep状态下的server状态
void led_temp(unsigned int led_map);//临时显示一些杂项，直接操作，尽快关闭，以免干扰正常显示
unsigned int LED_status(void);//返回实时的LED状态


void beep_sound_loop(void); //蜂鸣器鸣叫：启动时间，关闭时间，循环次数
void beep_sound(unsigned int on_time,unsigned int off_time,unsigned int total_cycles); //蜂鸣器鸣叫：启动时间，关闭时间，循环次数


#endif
