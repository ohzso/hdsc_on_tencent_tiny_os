#ifndef  _APP_KEY
#define  _APP_KEY


#include <rtthread.h>

void App_KEY(void* parameter);



#define STACK_SIZE_KEY  128
#define PRIORITY_KEY     12
#define TIMESLICE_KEY   100

#define KEY_IS_PRESSED			(Gpio_GetInputIO(GpioPortA, GpioPin4)==FALSE)

#define MAX_KEY_ARRAY				6		//一次输入最多由几次用户按压输入构成
#define MAX_KEY_VALUE				65530		//单次键值的最大值

#define	KEY_INPUT_STOP_LEN				300			//输入结束判定阈值，按钮release超过这个时间，则认为输入结束
#define KEY_DITHERING				5			//按钮抖动过滤时间（5* key_scan_interval)
#define KEY_SCAN_INTERVAL		2				//按钮状态刷新时间（2ms)

#define KEY_VALUE_UINT       424		//按钮持续1秒的增量，理论上1000ms/KEY_SCAN_INTERVAL = 500 ，实际上由于我的直观有误差，以及系统执行速度可能有问题，这个值比较符合直观


typedef struct _key_handler_
{
	unsigned int key_value[MAX_KEY_ARRAY];
	unsigned int key_value_arrar_pointer;//记录空位置的数组下标游标
	unsigned char input_start_flag;//输入周期开始标记
	unsigned char get_one_key_flag;//记录到一个键值元素输入完毕
	unsigned char data_valid;//有新的有效数据，数值等于有效数据个数，0表示无有效数据
	
	unsigned int dithering;//抖动参数（基本时间单位为1个扫描周期）
	unsigned int scan_interval;//扫描周期ms
	unsigned int key_release_time_real;//按钮持续松开计时实时
	unsigned int key_press_time_real;//按钮持续按压计时实时
	
	unsigned int key_release_time_filter;//按钮持续松开计时滤波
	unsigned int key_press_time_filter;//按钮持续按压计时滤波
	
	unsigned int key_value_saved;//记录的有效键值

	
	unsigned int key_status_real;//事实的按钮状态
	unsigned int key_status_filter;//滤波后的按钮状态
	unsigned int key_status_logic;//逻辑上的按钮状态
	unsigned int key_status_update;//逻辑按钮状态变化标记 0，无变化   1，按钮按下    2，按钮松开   3，按钮松开超时，一次输入完毕

} key_status_t;


//短按阈值
//3秒长按阈值
//5秒长按阈值


key_status_t *get_key_status(void);//获取键值
unsigned int clr_key_value(key_status_t *key_status);//清除键值

#endif
