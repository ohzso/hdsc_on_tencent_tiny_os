/******************************************************************************/
/** 文件： main.c
 ** 时间： 2021年2月26日  
 ** 作者： 何曾胜
 ** 版本： 1.0 
 ** 修改：
 ******************************************************************************/

#include "myInit.h"
#include <stdlib.h>
#include "tos.h"

k_task_t task;

k_stack_t task_stack[128];

void test_task(void *Parameter)
{
    while(1)
    {
//        printf("hello world!\r\n");
			
				Gpio_SetIO(GpioPortD, GpioPin5);//打开led

        tos_task_delay(1000);
			
				Gpio_ClrIO(GpioPortD, GpioPin5);//关闭led		

        tos_task_delay(1000);
			

				Wdt_Start();//开启看门狗，防止程序跑飞（跑飞

    }
}


int main(void)
{		
    k_err_t err;	      
		Wdt_Start();//开启看门狗，防止程序跑飞（跑飞
		my_init();
    printf("Welcome to TencentOS tiny\r\n");

    tos_knl_init(); // TOS Tiny kernel initialize
                                        
    err = tos_task_create(&task, 
                          "task1", 
                          test_task,
                          NULL, 
                          2, 
                          task_stack,
                          1024,
                          20);
    if(err != K_ERR_NONE)
        printf("TencentOS Create task fail! code : %d \r\n",err);
    
    tos_knl_start(); // Start TOS Tiny

}
//


//############################################MAIN.C文件结束#########################################
