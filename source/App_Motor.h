#ifndef  _MOTOR_CONTROL
#define  _MOTOR_CONTROL


#include <rtthread.h>


void Motor_Control(void* parameter);

#define Motor_Control_STACK_SIZE  128
#define Motor_Control_PRIORITY     75
#define Motor_Control_TIMESLICE   10

#define STR_MOTOR_ERR_TIME_OUT				"motor_runs_too_long_and_time_out"
#define STR_MOTOR_ERR_LIMIT						"motor_limit_sensor_status_error"

#define LEN_INTENT_LIST	10 //计划动作列表长度
#define MOTOR_CYCLE_TIME_MIN 150 //扫描循环最小时间常数ms(程序单次循环最少需要的时间）
#define m_time_fac (1000/MOTOR_CYCLE_TIME_MIN)		//计划时间(单位s) x m_time_fac = cycles


typedef enum _Valve_Status_
{	
	ValveOPENED,
	ValveCLOSED,
	ValveMIDDLE,//内部程序使用，外部不要使用
	ValveERROR,//内部程序逻辑使用，外部不要使用
} ENUM_VOLVE_STATUS_t;


typedef enum _Motor_Step_
{	
	M_Step_Brake,
	M_Step_Opening,
	M_Step_Opened,
	M_Step_Closing,
	M_Step_Closed,
} ENUM_MOTOR_STEP_t;


typedef enum _Motor_Control_Source_
{	
	Source_Local_user,
	Source_Local_sensor,
	Source_Remote_server,
	Source_Inner_error,
	Source_Program_Routine,
} ENUM_Motro_Ctr_Source_t;



typedef struct _MOTOR_INTENT_
{	
	ENUM_VOLVE_STATUS_t				intent;
	ENUM_Motro_Ctr_Source_t		source;
} ENUM_Motor_Intent_t;


typedef struct _MOTOR_HANDLER_
{
	ENUM_MOTOR_STEP_t 	current_step;				//当前步序
	ENUM_MOTOR_STEP_t 	prev_step;					//上一个步序
	ENUM_VOLVE_STATUS_t valve_status;				//阀当前状态
	ENUM_Motor_Intent_t	current_intent;			//当前计划状态
	ENUM_Motor_Intent_t	intent_list[LEN_INTENT_LIST];	//计划列表
	unsigned int				list_last;					//当前可用计划的位置（每次读取计划位置，都从此处读取）
	unsigned int				list_empty;					//空计划位置（在此插入追加的最新计划）	
	char								*err_str;						//故障信息 
	unsigned int				cycle_counter;			//程序循环次数，用于辅助不精确的计时延时等	
	unsigned int				sensor_counter;			//传感器信号到位计时，用于辅助不精确的计时延时等	
	unsigned int				motor_sleep_counter;	//电机睡眠时间计时器，用来辅助进行自动清洁，到位信号会复位这个计时器
	unsigned char				last_sensor_pos;		//上一个触发的传感器，用来辅助自动清洁，判断是否是有效的全行程运动，以清除睡眠计时
} MOTOR_HANDLER_t;


void Motor_open	(void);				//打开
void Motor_close(void);				//关闭
void Motor_brake(void);				//刹死
void Motor_free	(void);				//松开
int  Motor_sensor_opened(void);	//查询打开到位传感器状态0：未到位；1：到位；-1：错误
int  Motor_sensor_closed(void);	//查询关闭到位传感器状态


void Motor_para_init(MOTOR_HANDLER_t *motor_handler);//程序参数初始化

unsigned int get_next_list(unsigned int current,unsigned int len_of_list);//获取列表的下一个位置（到最大之后，返回最初）
int Intent_add(MOTOR_HANDLER_t *motor_handler,ENUM_VOLVE_STATUS_t intent,ENUM_Motro_Ctr_Source_t	source);//追加一个新动作位置
void Intent_set(MOTOR_HANDLER_t *motor_handler,ENUM_VOLVE_STATUS_t intent,ENUM_Motro_Ctr_Source_t	source);//立即执行新动作位置
int  Intent_next(MOTOR_HANDLER_t *motor_handler);//切换下一个动作命令 返回0，切换成功，返回-1，动作已执行完毕，列表已空
	
void M_step_brake_fun		(MOTOR_HANDLER_t *motor_handler);
void M_step_opening_fun	(MOTOR_HANDLER_t *motor_handler);
void M_step_opened_fun	(MOTOR_HANDLER_t *motor_handler);
void M_step_closing_fun	(MOTOR_HANDLER_t *motor_handler);
void M_step_closd_fun		(MOTOR_HANDLER_t *motor_handler);

unsigned int motor_cycles_to_ms(unsigned int cycles);//将电机循环的运行周期数转换为时间ms
unsigned int motor_cycles_to_s(unsigned int cycles);//将电机循环的运行周期数转换为时间s
unsigned int motor_ms_to_cycles(unsigned int time_ms);//将预期时间转换为电机循环的运行周期数
void 				 Motor_reset_sleep_counter(MOTOR_HANDLER_t *motor_handler);	//程序参数初始化
void 				 Motor_err_rst		(MOTOR_HANDLER_t *motor_handler);					//运行故障复位


extern MOTOR_HANDLER_t my_motor_handler;//电机控制结构体



#endif
