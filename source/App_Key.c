#include "App_Key.h"
#include "MYinit.h"


key_status_t my_key;

void App_KEY(void* parameter)
{	

	rt_base_t level;
	
	my_key.dithering=KEY_DITHERING;	//初始化滤波时间
	my_key.key_status_logic=FALSE;	//初始化为松开状态
	my_key.key_value_arrar_pointer=0;
	my_key.get_one_key_flag=FALSE;
	my_key.input_start_flag=FALSE;
	my_key.key_status_update=0;
	my_key.data_valid=0;
	
	
	while (1)
  {
			
			my_key.key_status_real=KEY_IS_PRESSED;//刷新实时按钮状态

/*-------------------实时的键值----------------------*/							
			if(my_key.key_status_real == TRUE)
			{
				if(my_key.key_press_time_real<MAX_KEY_VALUE)
					my_key.key_press_time_real ++;//按压时间计数增加
				my_key.key_release_time_real=0;//松开计时清零
			}
			else
			{
				my_key.key_press_time_real =0;//按压时间计数增加
				if(my_key.key_release_time_real<MAX_KEY_VALUE)
					my_key.key_release_time_real++;//松开计时清零			
			}
/*-------------------实时的键值----------------------*/						
		
			
/*-------------------滤波处理的键值----------------------*/						
			if(my_key.key_status_logic == FALSE)//松开状态 处理
			{	
			
				if(my_key.key_release_time_real<MAX_KEY_VALUE)		
					my_key.key_release_time_filter++;	
				
				if(my_key.key_press_time_real>my_key.dithering)
				{
					my_key.key_status_update=1;//按钮按下
					my_key.key_status_logic = TRUE;		
					my_key.key_press_time_filter=my_key.key_press_time_real;
					my_key.key_release_time_filter=0;		
											
				}
								
			}


			if(my_key.key_status_logic == TRUE)//按下状态 处理
			{			

		
				if(my_key.key_press_time_filter<MAX_KEY_VALUE)				
					my_key.key_press_time_filter++;	

				if(my_key.key_release_time_real>my_key.dithering)
				{
					my_key.key_status_update=2;//按钮松开
					my_key.key_status_logic = FALSE;		
					my_key.key_release_time_filter=my_key.key_release_time_real;
					
					my_key.key_value_saved=my_key.key_press_time_filter;//保存键值
					my_key.key_press_time_filter=0;	//键值清零
				}	
			}
			
			
			if(my_key.key_release_time_filter > KEY_INPUT_STOP_LEN)//空闲超时，说明一次输入结束
			{
				my_key.key_status_update=3;	//按钮超时，输入结束
			}
			
/*-------------------滤波处理的键值----------------------*/	


					
			
			if(	my_key.key_status_update !=0 )//检测到按钮状态有变化
			{
				
				switch(my_key.key_status_update)
				{
					case 1://按钮按下
						if(my_key.input_start_flag == FALSE)//判定输入开始
						{
							my_key.input_start_flag = TRUE;	
							
							level = rt_hw_interrupt_disable();
							my_key.data_valid=0;							
							rt_hw_interrupt_enable(level);
							
							my_key.key_value_arrar_pointer=0;//游标初始化，从第一个元素开始存入	


						}										
						break;
					case 2://按钮松开，存入新值
						if(my_key.input_start_flag == TRUE)
						{
							my_key.key_value[my_key.key_value_arrar_pointer]=my_key.key_value_saved;//存入最后一个值	
							if(my_key.key_value_arrar_pointer < MAX_KEY_ARRAY-1)//游标后移一位
								my_key.key_value_arrar_pointer++;
						}	
						break;
					case 3://按钮超时，判定输入结束
						if(my_key.input_start_flag == TRUE)//超时则输入结束
						{
							my_key.input_start_flag = FALSE;
							
							level = rt_hw_interrupt_disable();
							my_key.data_valid=my_key.key_value_arrar_pointer;
							rt_hw_interrupt_enable(level);

							
							for(;my_key.key_value_arrar_pointer < MAX_KEY_ARRAY;my_key.key_value_arrar_pointer++)//清零未用的部分
								my_key.key_value[my_key.key_value_arrar_pointer]=0;
							
						}
						break;
					default:
						break;
	
					
				}
				
				my_key.key_status_update=0;//已处理完毕
				
				
			}
			
			rt_thread_mdelay(KEY_SCAN_INTERVAL);//扫描间隙

			
			
	}//while 1 end
	

		
}		

unsigned int get_key_value(key_status_t *key_status)
{
}

unsigned int clr_key_value(key_status_t *key_status)
{
	rt_base_t level;
	
	level = rt_hw_interrupt_disable();
	key_status->data_valid=0;
	rt_hw_interrupt_enable(level);

	return 0;
}



key_status_t *get_key_status(void)//获取键值
{	
	return &my_key;	
}

