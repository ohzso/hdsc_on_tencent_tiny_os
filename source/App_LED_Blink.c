#include "App_LED_Blink.h"
#include "MYinit.h"

static unsigned char beep_on_time=10,beep_off_time=10,beep_cycles=1,beep_on_time_re=10,beep_off_time_re=10,current_beep_status=0;
//蜂鸣器启动时间、关闭时间，循环次数，启动重载时间，关闭重载时间,当前beep状态


static unsigned int led_status_buf[LED_BUF_LEN];
static unsigned int led_buf_sum=0;



void LED_Control(unsigned int led_remap);

void App_LED_Blink(void* parameter)
{	
			
	rt_thread_mdelay(1000);
  while (1)
  {				

		
		led_status_buf[LED_CLIENT]				=		client_leds();				//更新client 的状态灯		
		led_status_buf[LED_SLEEP_SERVER]	=		sleep_server_leds();	//更新sleep_server的状态灯
		
	
		LED_loop(led_status_buf,LED_BUF_LEN);	//LED灯	
		beep_sound_loop();//蜂鸣器
		
		rt_thread_mdelay(LED_Blink_CYCLE);
		
	}//while1结束
	
}


void LED_loop(unsigned int *led_buf,unsigned char buffer_len)//需要放在while1循环中，对所有buffer内容进行按位或运算，并最终显示在LED上
{
	unsigned int i;
	led_buf_sum=0;
	for(i =0;i < buffer_len;i++)
		led_buf_sum |= led_buf[i];	
	LED_Control(led_buf_sum);//最终进行显示
}

unsigned int LED_status(void)//返回实时的LED状态
{
	return led_buf_sum ;
}

void LED_Control(unsigned int led_remap)//最终进行显示
{
	(led_remap&(1<<3))?(Gpio_SetIO(GpioPortA, GpioPin5)):(Gpio_ClrIO(GpioPortA, GpioPin5));
	(led_remap&(1<<4))?(Gpio_SetIO(GpioPortA, GpioPin6)):(Gpio_ClrIO(GpioPortA, GpioPin6));
	(led_remap&(1<<5))?(Gpio_SetIO(GpioPortB, GpioPin10)):(Gpio_ClrIO(GpioPortB, GpioPin10));
	(led_remap&(1<<6))?(Gpio_SetIO(GpioPortB, GpioPin2)):(Gpio_ClrIO(GpioPortB, GpioPin2));
	(led_remap&(1<<7))?(Gpio_SetIO(GpioPortB, GpioPin11)):(Gpio_ClrIO(GpioPortB, GpioPin11));
	(led_remap&(1<<8))?(Gpio_SetIO(GpioPortA, GpioPin8)):(Gpio_ClrIO(GpioPortA, GpioPin8));
	(led_remap&(1<<9))?(Gpio_SetIO(GpioPortA, GpioPin9)):(Gpio_ClrIO(GpioPortA, GpioPin9));
	//(led_remap&(1<<10))?(Gpio_SetIO(GpioPortA, GpioPin10)):(Gpio_ClrIO(GpioPortA, GpioPin10));	改输入传感器占用
}



unsigned int client_leds(void)
{
		Enum_Device_status_t device_status;
		unsigned int led_map=0;

	
		device_status=get_water_leak_detector_status();

		switch(device_status)
		{
			case 	DEVICE_Initializing:
						led_map = 1<<3;break;
			case DEVICE_Wifi_Connect:
						led_map = 1<<4;break;
			case DEVICE_Server_Connect:
						led_map = 1<<5;break;
			case DEVICE_Send_Report:
						led_map = 1<<6;break;
			case DEVICE_Send_Settings:
						led_map = 1<<7;break;
			case DEVICE_listening:
						led_map = 1<<8;break;
			case DEVICE_Go_To_Sleep:
						led_map = 1<<9;break;
			case DEVICE_Smart_Config:
			{	
						if(get_water_leak_detector_handler()->factory_init_signal == TRUE)
							led_map = 7<<7;
						else
							led_map = 3<<8;
						break;
			}
			default:
				break;	
		}

		return led_map ;
	
}
//

void led_temp(unsigned int led_map)//临时显示一些杂项，直接操作，尽快关闭，以免干扰正常显示
{
	led_status_buf[LED_TEMP]=led_map;
}

unsigned int sleep_server_leds(void)
{
		Device_Handler_t *device_handler;
		Enum_Device_status_t device_status;
		unsigned int led_map=0;

	
		device_handler=get_water_leak_detector_handler();
		device_status=get_water_leak_detector_status();

		led_map=0;
	
		if(device_status==DEVICE_Go_To_Sleep)	//增加一层判断，减少正常状态下的运算量
		{
				switch(device_handler->sleep_server_handler.step)
				{
					case 1://初始化参数
								led_map = 1<<3;break;
					case 2://wifi重连与模式切换
								led_map = 1<<4;break;
					case 3://建立tcp侦听
								led_map = 1<<5;break;
					case 4://循环侦听消息，并检测服务器、wifi状态
								led_map = 1<<6;break;
					default:
						break;	
				}
		}

		return led_map ;
	
}








void beep_sound(unsigned int on_time,unsigned int off_time,unsigned int total_cycles) //蜂鸣器鸣叫：启动时间，关闭时间，循环次数
{
	rt_base_t level;
	level = rt_hw_interrupt_disable();
	
	beep_on_time=on_time;
  beep_off_time=off_time;
	beep_on_time_re=on_time;
	beep_off_time_re=off_time;
	beep_cycles=total_cycles;
	current_beep_status=1;//启动
	
	
	rt_hw_interrupt_enable(level);
}
	
void beep_sound_loop(void) //蜂鸣器鸣叫：需要放在while循环中，始终运行

{

	rt_base_t level;

	level = rt_hw_interrupt_disable();

	if(beep_cycles > 0)
	{
		
		switch(current_beep_status)//关闭中
		{
			case 0:
			{
				
				BEEP_OFF;
				if(beep_off_time>0)
					beep_off_time--;
				else
				{
					beep_on_time=beep_on_time_re;
					beep_cycles--;
					current_beep_status=1;//转到打开蜂鸣器
				}
				break;
				
			}
			
			case 1://打开中
			{
				BEEP_ON;

				if(beep_on_time>0)
					beep_on_time--;
				else
				{
					beep_off_time=beep_off_time_re;
					current_beep_status=0;//转到关闭蜂鸣器
				}
			 break;
			}	
		}
	}
//	else
//	{
//		BEEP_OFF;//关闭蜂鸣器，初始化各项参数
//		beep_cycles=0;
//		beep_off_time_re=0;
//		beep_on_time_re=0;
//	}
	rt_hw_interrupt_enable(level);

	
}





