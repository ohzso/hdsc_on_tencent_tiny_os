#ifndef __MYUART_H
#define __MYUART_H

#define ReadBufLen_Lpuart1 256   //低功耗串口的读取缓冲区长度
#define ReadBufLen_Lpuart0 256   //低功耗串口的读取缓冲区长度
#define Lpuart1_Line_Max		126		//Lpuart1一行最大的长度
#define Lpuart0_Line_Max		50		//Lpuart0一行最大的长度
#define TEMP_STR_BUF			200 	//临时存放一些字符串


#include "library.h"
#include "lpuart.h"
#include "uart.h"

void my_uart_init(void);

void my_lp_uart1_send( const char data);
unsigned int Luart1_getchar(void);//从接受buffer中获取一个字符
char*  Luart1_get_line();//接收一个完整的行(轮询且阻塞，未收到完整的行，将一直循环等待）
char*  Luart1_get_line_none_block(unsigned int str_length_max);//接收一个完整的行，不阻塞，无效直接返回0
void Luart1_flush(void);		//清空缓冲区；


extern char str_buf[TEMP_STR_BUF];		//存放无用返回字符串


//void my_lp_uart0_send( const char data);
//unsigned int Luart0_getchar(void);//从接受buffer中获取一个字符



#endif

