#ifndef __MYRTC_H__
#define __MYRTC_H__

#include "ddl.h"
#include "rtc.h"

void myRtcInit(void);
void myRtcFeed(void);


void myRtcread(uint8_t *time);
#endif

