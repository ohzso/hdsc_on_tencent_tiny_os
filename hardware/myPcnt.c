
/******************************************************************************
 * Include files
 ******************************************************************************/
#include  "Myinit.h"
#include  "MyPcnt.h"

extern en_result_t Pcnt_SetB2T(uint16_t value);
extern en_result_t Pcnt_SetB2C(uint16_t value);
extern en_result_t Pcnt_SetT2C(void);
extern boolean_t Pcnt_GetItStatus(en_pcnt_itfce_t IT_Src);
extern uint16_t Pcnt_GetCnt(void);
extern uint16_t Pcnt_GetTop(void);
extern uint16_t Pcnt_GetBuf(void);
extern void Pcnt_SetCnt(uint16_t value);
extern void Pcnt_SetBuf(uint16_t value);
extern void Pcnt_SetTop(uint16_t value);


int my_pcnt_cycles=0;


//PCNT 模块初始化
int my_pcnt_init(void)
{
	stc_pcnt_initstruct_t      temp_init_cfg;
	
	DDL_ZERO_STRUCT(temp_init_cfg);        
	
// 开启PCNT外设时钟，如需低功耗，需要切换时钟源至rcl 或xtl
	Sysctrl_SetPeripheralGate(SysctrlPeripheralPcnt, TRUE); 
	
// PCNT初始化配置	
	temp_init_cfg.Pcnt_Clk=PcntCLKPclk ;//低功耗模式下，需要更换为其它时钟源
	temp_init_cfg.Pcnt_ClkDiv=8000;			//滤波时钟的分频系数从1~8096 倍
	temp_init_cfg.Pcnt_DebTop=7;				//保持电平超过（FLT.DebTop）（1~7）个滤波时钟视为非抖动，可通过滤波器
	temp_init_cfg.Pcnt_Dir=PcntDirUp;		 //向上计数模式
	temp_init_cfg.Pcnt_FltEn=TRUE;
	temp_init_cfg.Pcnt_Mode=PcntSingleMode;		//单通道计数模式
	temp_init_cfg.Pcnt_S0Sel=PcntS0PNoinvert;	//通道极性不反转
	temp_init_cfg.Pcnt_S1Sel=PcntS1PNoinvert;	//通道极性不反转
	temp_init_cfg.Pcnt_TocrEn=FALSE;					//不开启超时功能
	temp_init_cfg.Pcnt_TocrTh=0xff;						//当脉冲高电平被滤波时钟连续采样计数达到阈值，产生超时中断标识，计数器重新开始计数
	Pcnt_Init(&temp_init_cfg);


	while(Pcnt_SetB2T(9) != Ok)				//设置PCNT的溢出值
		;
	while(Pcnt_SetB2C(0) != Ok)					//设置PCNT的初始计数值
		;

//中断配置，上溢出使能，正交模式下，需要同时开启下溢出中断	
	Pcnt_ItCfg(PcntOV, TRUE);
	
		
//开启ADC中断		
	EnableNvic(PCNT_IRQn,IrqLevel2,TRUE);        ///使能PCNTirq中断		
	Pcnt_Cmd(TRUE);	//启动运行		
	return 0;
		
}


void Pcnt_IRQHandler(void)
{
//	rt_interrupt_enter(); 	
/*
    PcntS1E            = 7,   // S1通道脉冲解码错误
    PcntS0E            = 6,   // S0通道脉冲解码错误   
    PcntBB             = 5,   // 脉冲解码错误    
    PcntFE             = 4,   // 采样周期脉冲解码错误
    PcntDIR            = 3,   // 反向改变中断    
    PcntTO             = 2,   // 超时中断标识   
    PcntOV             = 1,   // 上溢中断标识
    PcntUF             = 0,   // 下溢中断标识 
*/
	
	 if (TRUE == Pcnt_GetItStatus(PcntS1E))
	{
			Pcnt_ClrItStatus(PcntS1E);
	 
	}
	if (TRUE == Pcnt_GetItStatus(PcntS0E))
	{
			Pcnt_ClrItStatus(PcntS0E);

	}
	if (TRUE == Pcnt_GetItStatus(PcntBB))
	{
			Pcnt_ClrItStatus(PcntBB);
	
	}
	if (TRUE == Pcnt_GetItStatus(PcntFE))
	{
			Pcnt_ClrItStatus(PcntFE);

	}
	if (TRUE == Pcnt_GetItStatus(PcntDIR))
	{
			Pcnt_ClrItStatus(PcntDIR);

	}
	if (TRUE == Pcnt_GetItStatus(PcntTO))
	{
			Pcnt_ClrItStatus(PcntTO);
	}
	if (TRUE == Pcnt_GetItStatus(PcntOV))
	{
			Pcnt_ClrItStatus(PcntOV);
			my_pcnt_cycles++;	
	}
	if (TRUE == Pcnt_GetItStatus(PcntUF))
	{
			Pcnt_ClrItStatus(PcntUF);
			my_pcnt_cycles--;
	}	
	
	//清空中断标志位
	//user code
		
//	rt_interrupt_leave();			       
}




/******************************************************************************
 * EOF (not truncated)
 ******************************************************************************/

