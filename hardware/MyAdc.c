
/******************************************************************************
 * Include files
 ******************************************************************************/
#include  "Myinit.h"
#include 	"bgr.h"
#include  "myadc.h"

extern void Bgr_BgrEnable(void);
extern void Bgr_TempSensorEnable(void);
static unsigned int ch0,ch1,ch2,adc_ok=FALSE;


void MyAdcSQRCfg(void)
{

    M0P_ADC->SQR0_f.CH0MUX = AdcExInputCH2;//PA2
    M0P_ADC->SQR0_f.CH1MUX = AdcExInputCH2;//PA2
		M0P_ADC->SQR0_f.CH2MUX = AdcExInputCH2;//PA2
	
}


//< ADC 模块初始化
int my_adc_init(void)
{
    stc_adc_cfg_t              stcAdcCfg;
    stc_adc_sqr_cfg_t          stcAdcSqrCfg;
    
    DDL_ZERO_STRUCT(stcAdcSqrCfg);        
    DDL_ZERO_STRUCT(stcAdcCfg);
		
    
    // 开启ADC/BGR外设时钟
    Sysctrl_SetPeripheralGate(SysctrlPeripheralAdcBgr, TRUE); 
	
		Bgr_TempSensorEnable();	//打开内部温度模块
	
    Bgr_BgrEnable();     	 //开启BGR
    Adc_Enable();
			;
    // ADC 初始化配置
    stcAdcCfg.enAdcMode         = AdcScanMode;                  //采样模式-扫描
    stcAdcCfg.enAdcClkDiv       = AdcMskClkDiv8;                //采样分频-8
    stcAdcCfg.enAdcSampCycleSel = AdcMskSampCycle12Clk;         //采样周期数-12
    stcAdcCfg.enAdcRefVolSel    = AdcMskRefVolSelInBgr2p5;       		//AdcMskRefVolSelExtern1;       //参考电压选择内部2.5
    stcAdcCfg.enAdcOpBuf        = AdcMskBufEnable;             //OP BUF配置-关
    stcAdcCfg.enInRef           = AdcMskInRefDisable;        	 //内部参考电压使能-开
    stcAdcCfg.enAdcAlign        = AdcAlignRight;                //转换结果对齐方式-右
    Adc_Init(&stcAdcCfg);
	 
    // 顺序扫描模式功能及通道配置
    //注意：扫描模式下，当配置转换次数为n时，转换通道的配置范围必须为[SQRCH(0)MUX,SQRCH(n-1)MUX]
    stcAdcSqrCfg.bSqrDmaTrig = FALSE;
    stcAdcSqrCfg.enResultAcc = AdcResultAccDisable;
    stcAdcSqrCfg.u8SqrCnt    = 3;
    Adc_SqrModeCfg(&stcAdcSqrCfg);
		
		MyAdcSQRCfg();//通道设定,必须放在bgr和adc启动后
		
		Adc_EnableIrq();//开启ADC中断
		
		EnableNvic(ADC_IRQn,IrqLevel2,TRUE);        ///是能ADCirq中断
		
		Adc_SQR_Start();
		
		return 0;
		
}

void Adc_IRQHandler(void)
{
//	rt_interrupt_enter();
	
	Adc_GetIrqStatus(AdcMskIrqSqr);
	Adc_ClrIrqStatus(AdcMskIrqSqr);
	ch0=Adc_GetSqrResult(AdcSQRCH0MUX);
	ch1=Adc_GetSqrResult(AdcSQRCH1MUX);
	ch2=Adc_GetSqrResult(AdcSQRCH2MUX);
	adc_ok=TRUE;
//	Adc_SQR_Start();
	
//	rt_interrupt_leave();
}

unsigned int GetADC_value(unsigned int channel)
{
	if (channel ==0)
		return ch0;
	if(channel == 1)
		return ch1;
	if(channel == 2)
		return ch2;
}

unsigned int ClearADC_value(unsigned int channel)
{
	if (channel ==0)
		ch0=0;
	if(channel == 1)
		ch1=0;
	if(channel == 2)
		ch2=0;
}

unsigned int ADC_START(void)
{
	adc_ok=FALSE;
	Adc_SQR_Start();
}

unsigned int ADC_GET_FLAG(void)
{
	return adc_ok;
}


/******************************************************************************
 * EOF (not truncated)
 ******************************************************************************/

