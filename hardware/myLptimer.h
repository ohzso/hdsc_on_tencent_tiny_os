#ifndef __MYLPTIM_H_
#define __MYLPTIM_H_

#include "lptim.h"


#define Max_Timers 50 //定义软件定时器个数，数量越少，对系统影响越小，当定时器的值不等于0时，会自动减，直至到0

/*
软定时器的特点：
1：非阻塞式的，启动之后，不会使线程挂起，可以继续运行之后的程序，只需在必要的地方检查时间即可
2：无需操作系统支持，可以在rt_thread启动前使用
3：在中断中也能用，但不可持续等待（可能会造成锁死）

推荐使用方法：
1：用start_timers()启动定时器
2：用get_timers()轮询检查，看时间是否已到
3：不要持续等待时间到，在线程中，while(timer) 会造成低优先级线程在这段时间里无法被调用，
	 推荐采用，或在循环里嵌套rt_delayms(x);
	 在高优先级中断里，while（timer）可能会因timer无法更新，造成系统锁死.
*/

int my_lptimer_init(void);

unsigned int start_timers(unsigned int time_index,unsigned int time);//获取指定序号软定时器的序号
unsigned int get_timers(unsigned int time_index);//获取指定序号软定时器的时间

#endif
