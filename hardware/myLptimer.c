
/******************************************************************************
 * Include files
 ******************************************************************************/
#include  "Myinit.h"

#include  "MyLptimer.h"

static unsigned int my_timers[Max_Timers];//定义Max_Timers个软timer,这些timer不等于0时，会自动慢慢减到0，分辨率根据lptimer的周期，100ms时，则最大约13年


//PCNT 模块初始化
int my_lptimer_init(void)
{
		unsigned int i_;

		stc_lptim_cfg_t    temp_cfg;
	  uint16_t           u16ArrData = 65535-3276;//100ms 左右 //
	
    DDL_ZERO_STRUCT(temp_cfg);
	
		
		for(i_=0;i_< Max_Timers;i_++)
			my_timers[i_]=0;
			

    Sysctrl_SetPeripheralGate(SysctrlPeripheralLpTim, TRUE);

    temp_cfg.enGate   = LptimGateLow;
    temp_cfg.enGatep  = LptimGatePLow;
    temp_cfg.enTcksel = LptimRcl;//内部低速时钟
    temp_cfg.enTogen  = LptimTogEnLow;
    temp_cfg.enCt     = LptimTimerFun;         
    temp_cfg.enMd     = LptimMode2;   //16位重载模式        
    temp_cfg.u16Arr   = u16ArrData;                     
    Lptim_Init(M0P_LPTIMER, &temp_cfg);

    Lptim_ClrItStatus(M0P_LPTIMER); 
  
    Lptim_ConfIt(M0P_LPTIMER, TRUE);  //开启外设中断
    EnableNvic(LPTIM_IRQn, IrqLevel3, TRUE);//中断管理器内的相应设置
		Lptim_Cmd(M0P_LPTIMER, TRUE) ;// 启动定时器
	
}


void LpTim_IRQHandler(void)
{
	unsigned int _temp_i;
//	rt_uint32_t level;
//	rt_interrupt_enter(); 
	
	
	
  if (TRUE == Lptim_GetItStatus(M0P_LPTIMER))
     {
		      
          Lptim_ClrItStatus(M0P_LPTIMER);
			 
				/*更新软定时器*/				 
//					level = rt_hw_interrupt_disable();//临时关闭中断，防止数据同时修改
					for(_temp_i=0;_temp_i<Max_Timers;_temp_i++)
					{
						if(my_timers[_temp_i]>0)
							my_timers[_temp_i]--;	
					}
//					rt_hw_interrupt_enable(level);	//打开中断。
					
				/*更新软定时器*/		 
						 
		}		 
		 	 
		 
//	rt_interrupt_leave();			       
}


unsigned int get_timers(unsigned int time_index)//获取指定序号软定时器的时间
{
	if(time_index<Max_Timers)
		return my_timers[time_index];
	else
		return ~0;	
}

unsigned int start_timers(unsigned int time_index,unsigned int time)//获取指定序号软定时器的序号
{
//	rt_uint32_t level;
	
	if(time_index<Max_Timers)
	{
//		level = rt_hw_interrupt_disable();//临时关闭中断，防止数据同时修改
		my_timers[time_index]=time;
//		rt_hw_interrupt_enable(level);	//打开中断，可以继续使用。
	}
	else
		return ~0;//超范围，报错
	return 0;	
}



/******************************************************************************
 * EOF (not truncated)
 ******************************************************************************/

