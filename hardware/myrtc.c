#include "rtc.h"
#include "myinit.h"
 

/**
******************************************************************************
    ** \brief  RTC中断入口函数
    ** 
  ** @param  无
    ** \retval 无
    **
******************************************************************************/  

void Rtc_IRQHandler(void)
{
   if(Rtc_GetPridItStatus() == TRUE)//周期中断
    {
			 Rtc_ClearPrdfItStatus();             //清除中断标志位
		   Wdt_Feed();
//user code

    }

}

/**
******************************************************************************
  ** \brief  配置RTC
  **
  ** @param  无
  ** \retval 无
  **
******************************************************************************/
void myRtcInit(void)
{
    stc_rtc_initstruct_t RtcInitStruct;
    stc_rtc_alarmtime_t RtcAlmStruct;

    DDL_ZERO_STRUCT(RtcInitStruct);                      //变量初始值置零
    DDL_ZERO_STRUCT(RtcAlmStruct);

    Sysctrl_SetPeripheralGate(SysctrlPeripheralRtc,TRUE);//RTC模块时钟打开
	
    RtcInitStruct.rtcAmpm = RtcPm;                       //24小时制
    RtcInitStruct.rtcClksrc = RtcClkRcl;//RtcClkXtl;                 //外部低速时钟
    RtcInitStruct.rtcPrdsel.rtcPrdsel = RtcPrds;         //周期中断类型PRDx
    RtcInitStruct.rtcPrdsel.rtcPrdx = 0;                //周期中断时间间隔 1s
    RtcInitStruct.rtcTime.u8Second = 0x00;               //配置RTC时间2020年7月07日14:10:20
    RtcInitStruct.rtcTime.u8Minute = 0x30;
    RtcInitStruct.rtcTime.u8Hour   = 0x14;
    RtcInitStruct.rtcTime.u8Day    = 0x02;
 //   RtcInitStruct.rtcTime.u8DayOfWeek =0x14;
    RtcInitStruct.rtcTime.u8Month  = 0x04;
    RtcInitStruct.rtcTime.u8Year   = 0x21;
    RtcInitStruct.rtcCompen = RtcCompenEnable;           // 使能时钟误差补偿
    RtcInitStruct.rtcCompValue = 0;                      //补偿值  根据实际情况进行补偿
    Rtc_Init(&RtcInitStruct);

//    RtcAlmStruct.RtcAlarmMinute = 0x12;
//    RtcAlmStruct.RtcAlarmHour = 0x15;
//    RtcAlmStruct.RtcAlarmWeek = 0x7f;                    //从周一到周日，每天15:12:00启动一次闹铃
//    Rtc_SetAlarmTime(&RtcAlmStruct);                     //配置闹铃时间
//    Rtc_AlmIeCmd(TRUE);                                  //使能闹钟中断

    EnableNvic(RTC_IRQn, IrqLevel2, TRUE);               //使能RTC中断向量
    Rtc_Cmd(TRUE);                                       //使能RTC开始计数
}




