#include "MyFlash.h"
#include "flash.h"

eeprom_handler_t my_eeprom_handler;

int my_flash_init(void)
{	
	
	eeprom_cfg_t eeprom_cfg;
	
	Flash_Init(6, TRUE );//24Mhz模式
	Flash_WaitCycle(FlashWaitCycle0);//小于等于24Mhz时间等待
		
	eeprom_cfg.base_address  	=	BASE_ADDR_A;
	eeprom_cfg.number_of_tags	=	MAX_OF_TAGS;
	eeprom_cfg.page_size			=	PAG_SIZE ;	
	
	my_eeprom_format(&eeprom_cfg,&my_eeprom_handler) ;//格式化检查，如果未格式化，则进行格式化,并写入格式化完毕标记0x0f
	
	my_eeprom_init(&eeprom_cfg ,&my_eeprom_handler);
		
	return 0;
}

unsigned int my_flash_read(unsigned int addr)
{
	return *((unsigned int *)addr);
}


enum PAGE_TAG	current_page(const eeprom_handler_t * handler)	//找出并返回当前使用中的页（A或B）
{
	FLASH_STATUS_t * flash_statusA, * flash_statusB;
	
	flash_statusA=(FLASH_STATUS_t * )(handler->base_address_A);	//找出flashstatusA的结构体地址
	flash_statusB=(FLASH_STATUS_t * )(handler->base_address_B);	//找出flashstatusB的结构体地址
	
	if((flash_statusA->conuter) > (flash_statusB->conuter))			//格式化后为0xffffffff，每次转移，这个counter都会在前一个的基础上减1，所以越小，说明越新
		return FLASH_PAGE_B;
	else
		return FLASH_PAGE_A;
}



int my_eeprom_format( const eeprom_cfg_t *	cfg,eeprom_handler_t *handler_0)//强制格式化flash区域（检查flash，未格式化，则重新格式化）
{
	eeprom_handler_t  *handler, handler_temp;	
	handler=&handler_temp;
	unsigned int counterA,counterB;
	int err=0;
	
	handler->base_address_A			=cfg->base_address;
	handler->base_address_B			=cfg->base_address + cfg->page_size;
	handler->max_number_of_tags	=cfg->number_of_tags;
	handler->page_size     			=cfg->page_size;
	handler->error         			=0;
	
	
	counterA = ((FLASH_STATUS_t*)(handler->base_address_A))->conuter;
	counterB = ((FLASH_STATUS_t*)(handler->base_address_B))->conuter;
	
	if((counterA==0xffffffff) && (counterB==0xffffffff)) //说明flash未初始化
	{
			Flash_SectorErase(handler->base_address_A);//扇区A起始地址触发整个扇区的擦除
			Flash_SectorErase(handler->base_address_B);//扇区B起始地址触发整个扇区的擦除
			
			Flash_WriteWord((unsigned int)(&((FLASH_STATUS_t*)(handler->base_address_A))->conuter),0xfffffffe);//扇区A的conuter地址写入一个小点的值，使其默认优先级更高
			Flash_WriteByte((unsigned int)(&((FLASH_STATUS_t*)(handler->base_address_A))->flag_using),PAGE_WRITE_OVER);		//扇区A的flag_using标记为：掉电切换标记完成

			Flash_WriteWord((unsigned int)(&((FLASH_STATUS_t*)(handler->base_address_B))->conuter),0xffffffff);//扇区A的conuter地址写入一个小点的值，使其默认优先级更高
			Flash_WriteByte((unsigned int)(&((FLASH_STATUS_t*)(handler->base_address_B))->flag_using),PAGE_WRITE_OVER);		//扇区A的flag_using标记为：掉电切换标记完成			
			err=1;
		  handler_0->format =0x0f;
	}
	return err;	
}





/*
检查并更正falsh的两个page切换过程中掉电引发的错误，
每次上电初始化中使用一次,
原理：每次页面切换完毕，会将status_flag变量写如0x01,
由于它是最后一个写入flash的值（仅次于counter之后），如果它成功改写，说明之前的也在掉电前改写完毕。
counter改写过程中失败，一般情况下，会偏大，所以重新使用中，优先级不高，会在正常使用中自然触发重写。
*/
int flash_eeprom_pwron_check(const eeprom_handler_t * handler)
{
	FLASH_STATUS_t * flash_status;
	enum PAGE_TAG page_tag;
	
	page_tag=current_page(handler);
	
	if(page_tag==FLASH_PAGE_A)
	{//A页面为高优先级
		
		
			flash_status=(FLASH_STATUS_t *)(handler->base_address_A);
			
			if((flash_status->flag_using)<PAGE_WRITE_MIDD)
			{
				return 0;//如果高优先级页面的写入标记正常，则意味着一切正常
			}
			else
			{
				flash_eeprom_page_switch(handler,FLASH_PAGE_A);//否则将B页面重新转移至A页面中	
				return 1;
			}
			
		
	}	
	else//B页面为高优先级
	{
		
			flash_status=(FLASH_STATUS_t *)(handler->base_address_B);
			
			if((flash_status->flag_using)<PAGE_WRITE_MIDD)
			{
				return 0;//如果高优先级页面的写入标记正常，则意味着一切正常
			}
			else
			{
				flash_eeprom_page_switch(handler,FLASH_PAGE_B);//否则将A页面重新转移至B页面中
				return 1;
			}		
	}	
	
	return 2;//正常不会执行到这里
}




int flash_eeprom_page_switch(const eeprom_handler_t * handler,enum PAGE_TAG to)//切换并压缩数据至备份页
{	
	FLASH_ENTRY_t *p_entry_from=NULL,*p_entry_to=NULL;
	unsigned int end_address_of_from,end_address_of_to,try_counter=0;//from页的终止地址，to页的终止地址
	
	if(to==FLASH_PAGE_A)
	{
		p_entry_to	=(FLASH_ENTRY_t *)(handler->base_address_A+sizeof(FLASH_STATUS_t));	
		end_address_of_to		=handler->base_address_A+handler->page_size-1;
		
		p_entry_from=(FLASH_ENTRY_t *)(handler->base_address_B+sizeof(FLASH_STATUS_t));
		end_address_of_from	=handler->base_address_B+handler->page_size-1;
	}
	else
	{
		p_entry_to	=(FLASH_ENTRY_t *)(handler->base_address_B+sizeof(FLASH_STATUS_t));
		end_address_of_to		=handler->base_address_B+handler->page_size-1;
		
		p_entry_from=(FLASH_ENTRY_t *)(handler->base_address_A+sizeof(FLASH_STATUS_t));
		end_address_of_from	=handler->base_address_A+handler->page_size-1;
	}
	
	
	Flash_SectorErase((unsigned int)p_entry_to);//触发目的扇区的擦除
	
	while(  ( (unsigned int)p_entry_from  <  end_address_of_from  )&&( (unsigned int)p_entry_to < end_address_of_to )  )
	{
		
		if(p_entry_from->flag == ENTRY_VALID)
		{
			
/*		
				try_counter=0;
				do{
					Flash_WriteByte((unsigned int)(&(p_entry_to->flag	)),ENTRY_PGING);//写入正在写入标记，占住这一个空间
				}while((p_entry_to->flag!=ENTRY_PGING)&&(try_counter<TRY_TIMS));			
			
				if(try_counter >= TRY_TIMS)
					return -1;		
*/					
				try_counter=0;			
				do{
						Flash_WriteWord((unsigned int)(&(p_entry_to->data	)),p_entry_from->data	);//写入数据
					}while((p_entry_to->data!=p_entry_from->data)&&(try_counter<TRY_TIMS));
				
				if(try_counter >= TRY_TIMS)
				return -1;
					
				try_counter=0;
				do{
						Flash_WriteByte((unsigned int)(&(p_entry_to->tag	)),p_entry_from->tag 	);//写入Tag
					}while((p_entry_to->tag!=p_entry_from->tag)&&(try_counter<TRY_TIMS));
				
					
				try_counter=0;
				do{
					Flash_WriteByte((unsigned int)(&(p_entry_to->flag	)),ENTRY_VALID				);//写入有效标记，启用这一条目tag
				}while((p_entry_to->flag!=p_entry_from->flag)&&(try_counter<TRY_TIMS));		
	
				
				if(try_counter >= TRY_TIMS)
					return -1;
					
			//		Flash_WriteByte((unsigned int)(&(p_entry_to->type	)),p_entry_from->type	);//写入数据类型 	暂时不用
			//		Flash_WriteByte((unsigned int)(&(p_entry_to->crc	)),p_entry_from->crc	);//写入校验值		暂时不用
					
					
				
					
					
			p_entry_to++;//下一个目标位置
		}	
		p_entry_from++;//下一个源位置			
	}
	
	if(end_address_of_to < (unsigned int)p_entry_to)	
		return -1;//运行到此处，说明数据没有转移完，转移失败了。
	
	Flash_WriteWord((unsigned int)(&(((FLASH_STATUS_t *)(end_address_of_to-(handler->page_size)+1))->conuter)),((FLASH_STATUS_t *)(end_address_of_from-(handler->page_size)+1))->conuter-1);//写入上次反转次数-1
	Flash_WriteWord((unsigned int)(&(((FLASH_STATUS_t *)(end_address_of_to-(handler->page_size)+1))->flag_using)),PAGE_WRITE_OVER);//写入切换完毕标记

	
	
}


//根据cfg的参数，对handler内容进行完善，并调用check函数，决定是否需要格式化（先实现掉电切换错误纠正，不格式化）
int my_eeprom_init( const eeprom_cfg_t *cfg,eeprom_handler_t *handler)
{
	
	handler->base_address_A			=cfg->base_address;
	handler->base_address_B			=cfg->base_address + cfg->page_size;
	handler->max_number_of_tags	=cfg->number_of_tags;
	handler->page_size     			=cfg->page_size;
	handler->error         			=0;
	
	flash_eeprom_pwron_check(handler);
	
	flash_eeprom_update_active_address(	handler);//上电初始化handlerd active标记，必须在pwron_check之后
	return 0;
}



int flash_eeprom_entry_write(eeprom_handler_t *handler,unsigned int data,unsigned char tag)
{	
/* 写入一个基本条目，无换页功能，必须保证前期已判断好空位，再调用此函数	*/	
	
	FLASH_ENTRY_t *p_entry=NULL;
	unsigned int end_address,error_=0,try_counter=0;//当前页最后一个地址,错误码，尝试计数	
	
	
	//查找并删掉重复tag	
	flash_eeprom_update_active_address(	handler);		
	p_entry			= (FLASH_ENTRY_t *)(handler->active_top_address);//数据区起始地址
	end_address	=										handler->current_empty ;//使用过的数据区最终地址(不删掉刚写的数据）
	
	

	for(;(((unsigned int)p_entry) < end_address);p_entry++)
	{
		if((p_entry->flag == 	ENTRY_VALID) && (p_entry->tag == tag ) && (p_entry->data==data) )//判断，如果数据没变，则直接返回成功,不重复写入
			return 0;
	
	}
	
	

	try_counter=0;
	do{
			try_counter++;
			Flash_WriteByte((unsigned int)(&(((FLASH_ENTRY_t *)(handler->current_empty))->flag))	,ENTRY_PGING);//写入正在写入标记，占住这一个空间
		}
	while( ((((FLASH_ENTRY_t *)(handler->current_empty))->flag)!= ENTRY_PGING)&&(try_counter < TRY_TIMS) );
	
	if(try_counter >= TRY_TIMS)
		return -1;
		
		
	//单页内写入一个条目，无页面切换功能,此时handler->empty内的地址必须有效	
	//写入条目（数据）
	do{
			try_counter++;
			Flash_WriteWord((unsigned int)(&(((FLASH_ENTRY_t *)(handler->current_empty))->data))	,data				);
			Flash_WriteByte((unsigned int)(&(((FLASH_ENTRY_t *)(handler->current_empty))->tag))		,tag				);
			Flash_WriteByte((unsigned int)(&(((FLASH_ENTRY_t *)(handler->current_empty))->flag))	,ENTRY_VALID);	
		}
		while(\
						(\
								((((FLASH_ENTRY_t *)(handler->current_empty))->data) 	!= data)				\
							||((((FLASH_ENTRY_t *)(handler->current_empty))->tag)		!= tag)					\
							||((((FLASH_ENTRY_t *)(handler->current_empty))->flag)	!= ENTRY_VALID)	\
						)\
						&&(try_counter<TRY_TIMS)
					);	
	
	if(try_counter>=TRY_TIMS)
	{
		handler->error=handler->current_empty;//故障次数太多，将此地址写入故障代码
		return -1;
	}
	
	

	p_entry			= (FLASH_ENTRY_t *)(handler->active_top_address);//重新指向数据区起始地址
	for(;(((unsigned int)p_entry) < end_address);p_entry++)
	{
		if((p_entry->flag == 	ENTRY_VALID) &&(p_entry->tag == tag ))
			Flash_WriteByte((unsigned int)(&(p_entry->flag)),ENTRY_ERASE);//逆序读取，顺序擦除，此时掉电也不影响。	
	}
		
//循环写入并检查，直到正确（坏点可能会卡死程序,更好的写法是，）
	
	return 0; 

}



int my_eeprom_delet(eeprom_handler_t *handler,unsigned char tag)
{
	FLASH_ENTRY_t *p_entry=NULL;
	unsigned int end_address;//当前页最后一个地址
	
	//查找并删掉重复tag	
	flash_eeprom_update_active_address(	handler);		
	p_entry			= (FLASH_ENTRY_t *)(handler->active_top_address);
	end_address	=										handler->active_end_address ;	

	for(;(((unsigned int)p_entry) < end_address);p_entry++)
	{
		if((p_entry->flag == 	ENTRY_VALID) &&(p_entry->tag == tag ))
		{	
				Flash_WriteByte((unsigned int)(&(p_entry->flag)),ENTRY_ERASE);//写入删除标记
		}	
	}

	return 1; //出错则返回最大值

}

/*
由eeprom_read()函数调用，查找最新空位,并更新地址指针。
返回一条空地址，并更新当前页最新空地址
返回PAGE_FULL_NOT：正常 
返回PAGE_FULL_PHY：已满，可换页；
返回PAGE_FULL_LOG 已满，无逻辑空间
*/

enum PAGE_STAT flash_eeprom_updata_alloc(eeprom_handler_t *handler)
{
	
	FLASH_ENTRY_t *p_entry=NULL;
	unsigned int end_address,counter_tags=0;//当前页最后一个地址,有效tag的计数器
	
	flash_eeprom_update_active_address(	handler);	
	//更新当前活动页数据区的起止地址（page_switch也会改变这个值）
	
	p_entry			= (FLASH_ENTRY_t *)(handler->active_top_address);
	end_address	=										handler->active_end_address ;
	
//如果刚上电，current_empty还是0，但是实际flash内有内容，需要顺序遍历检查，更新内容	
	for(;(((unsigned int)p_entry) < end_address);p_entry++)
	{
		if(p_entry->flag 	== 	ENTRY_VALID )
		{			
			counter_tags++;	
		}	
		
		if(counter_tags 	>= 	MAX_OF_TAGS)//逻辑地址用完,无法继续分配变量
			return PAGE_FULL_LOG;	
		
		if(p_entry->flag 	== 	ENTRY_EMPTY)//找到空地址，返回给控制句柄
		{
			handler->current_empty=(unsigned int)p_entry;
			return PAGE_FULL_NOT;
		}	
		
	}
	
	return PAGE_FULL_PHY;	//物理地址用完了，则需要换页
}


unsigned int	my_eeprom_read	(	eeprom_handler_t *handler,unsigned char tag)
{

	FLASH_ENTRY_t *p_entry=NULL;

	unsigned int start_address;//当前页最后一个地址

	flash_eeprom_update_active_address(	handler);
		
	p_entry					=(FLASH_ENTRY_t *)(handler->active_end_address+1-sizeof(FLASH_ENTRY_t));
	start_address		= 							   handler->active_top_address;
	
	for(;(((unsigned int)p_entry) >= start_address);p_entry--)
	{
		if((p_entry->flag == 	ENTRY_VALID) &&(p_entry->tag == tag ))
		{				
				return p_entry->data;	
		}	
	}	

	
/*	
//顺序查找，可靠度低	
	FLASH_ENTRY_t *p_entry=NULL;
	unsigned int end_address;//当前页最后一个地址

	flash_eeprom_update_active_address(	handler);
		
	p_entry			=		(FLASH_ENTRY_t *)(handler->active_top_address);
	end_address	= 										handler->active_end_address ;
	
	
	for(;(((unsigned int)p_entry) < end_address);p_entry++)
	{
		if((p_entry->flag == 	ENTRY_VALID) &&(p_entry->tag == tag ))
		{				
				return p_entry->data;	
		}	
	}
	
 */
 
	handler->error=ERROR_TAG_NOT_FOUND;//	出错标记
	return (~0x0); //出错则返回最大值
}


int flash_eeprom_update_active_address(	eeprom_handler_t *handler	)
{
	/*找出实际正在使用的页面数据区的起止地址*/
	FLASH_ENTRY_t *p_entry=NULL;
	unsigned int end_address,counter_tags=0;//当前页最后一个地址,有效tag的计数器
	
	if(current_page(handler)==FLASH_PAGE_A)	
	{
		p_entry=(FLASH_ENTRY_t *)(handler->base_address_A+sizeof(FLASH_STATUS_t));		
		end_address=handler->base_address_A+handler->page_size-1;	
	}		
	else			
	{	
		p_entry=(FLASH_ENTRY_t *)(handler->base_address_B+sizeof(FLASH_STATUS_t));	
		end_address=handler->base_address_B+handler->page_size-1;	
	}	
	
//更新当前活动页数据区的起止地址（entry_write page_switch会引起这个值改变）	
	handler->active_top_address=(unsigned int)p_entry;
	handler->active_end_address=end_address;

	return 0; //出错则返回最大值
}



int my_eeprom_write	( eeprom_handler_t *handler,unsigned char tag,unsigned int	data)
{
	int error=0;//用来记录换页是否成功
	
	switch (flash_eeprom_updata_alloc(handler))
	{	
			case PAGE_FULL_LOG :
			{
				return -2;
				break;
			}
			
			case PAGE_FULL_PHY :
			{
				
				if(current_page(handler)==FLASH_PAGE_A)
					error=flash_eeprom_page_switch(handler,FLASH_PAGE_B);
				else
					error=flash_eeprom_page_switch(handler,FLASH_PAGE_A);
				flash_eeprom_updata_alloc(handler);//page switch 之后，数据被压缩，必然有空位，所以不必再判断，直接进入下一步Page not full(暂不考虑写入失败)
				
				if(error==-1)//换页失败，直接故障返回（目前换页函数不检查失败，暂时无用）
					return -1;
			}		
			case PAGE_FULL_NOT :
			{
				flash_eeprom_entry_write(handler,data,tag);//正常写入数据
				break;
			}
			default :
			{	
				break;
			}
	}
	return 0;	
}









/****************************************debug辅助代码段*********start**********************/

	
/*将固定地址打印出来		
		key_value=my_flash_read(64224+4);
		rt_kprintf("Flash:%d\n",key_value);
*/



/*将整个扇区打印出来，用来检查数据移动		
		rt_kprintf("\nPAGE-AAAAAAAAAAAAAAAAAAAAAAAAAAAA_TOP%\n");	
		for(i=BASE_ADDR_A;i<BASE_ADDR_B;i+=4,rt_kprintf("|"))
		{	
			rt_kprintf("%X",my_flash_read	(i));
			rt_thread_mdelay(1);
		}		
		rt_kprintf("\nPAGE_AAAAAAAAAAAAAAAAAAAAAAAAAAAA_END%\n");
		
		rt_kprintf("\nPAGE-BBBBBBBBBBBBBBBBBBBBBBBBBBBB_TOP%\n");		
		for(i=BASE_ADDR_B;i<BASE_ADDR_B+PAG_SIZE;i+=4,rt_kprintf("|"))
		{	
			rt_kprintf("%X",my_flash_read	(i));
			rt_thread_mdelay(1);
		}	
		rt_kprintf("\nPAGE_BBBBBBBBBBBBBBBBBBBBBBBBBBBB_END%\n");
*/



/*	清零整个扇区					
							Flash_SectorErase(64000);	
							for(i=0;i<128;i++)
								Flash_WriteWord(64000+4*i,0);
*/



/*打印handler内容
		rt_kprintf("TOP:%d END:%d PA:%d PB:%d SIZE:%d EMPTY:%d maxTAG:%d\n",\
								my_eeprom_handler.active_top_address,\
								my_eeprom_handler.active_end_address,\
								my_eeprom_handler.base_address_A,\
								my_eeprom_handler.base_address_B,\
								my_eeprom_handler.page_size,\
								my_eeprom_handler.current_empty,\
								my_eeprom_handler.max_number_of_tags\
							);	
*/	


/****************************************debug辅助代码段**********end***********************/






