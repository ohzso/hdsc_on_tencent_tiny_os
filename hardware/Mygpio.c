
#include "Mygpio.h"

void gpio_io_init(void)
{
	  stc_gpio_cfg_t tempGpioCfg;
	

		DDL_ZERO_STRUCT(tempGpioCfg);	
    tempGpioCfg.enDir = GpioDirIn; 
		tempGpioCfg.enDrv = GpioDrvL; 
    tempGpioCfg.enPu = GpioPuEnable;
		tempGpioCfg.enPd = GpioPdDisable;
    tempGpioCfg.enCtrlMode = GpioAHB;
    Gpio_Init(GpioPortD, GpioPin4, &tempGpioCfg); //PD4 Key1



	
		DDL_ZERO_STRUCT(tempGpioCfg);	        
    tempGpioCfg.enDir = GpioDirOut;
    tempGpioCfg.enDrv = GpioDrvH; 
    tempGpioCfg.enPu = GpioPuDisable;
		tempGpioCfg.enPd = GpioPdDisable;
    tempGpioCfg.enOD = GpioOdDisable;
    tempGpioCfg.enCtrlMode = GpioAHB;
    Gpio_Init(GpioPortD, GpioPin5, &tempGpioCfg); //PD05 LED
		Gpio_ClrIO(GpioPortD, GpioPin5);
	
	
}

//===================================================================================

//===================================================================================
void gpio_uart_init(void)
{
//Lpuart 1
		stc_gpio_cfg_t stcGpioCfg;

    DDL_ZERO_STRUCT(stcGpioCfg);     
    ///<TX
	  stcGpioCfg.enPu = GpioPuEnable;
    stcGpioCfg.enDir =  GpioDirOut;
    Gpio_Init(GpioPortA,GpioPin0,&stcGpioCfg);
    Gpio_SetAfMode(GpioPortA,GpioPin0,GpioAf2); //PA00 LPUART1_TX
    //<RX
	  stcGpioCfg.enPu = GpioPuEnable;
    stcGpioCfg.enDir =  GpioDirIn;
    Gpio_Init(GpioPortA,GpioPin1,&stcGpioCfg);
    Gpio_SetAfMode(GpioPortA,GpioPin1,GpioAf2); //PA01 LPUART1_RX



/*	
//lpuart0	
    DDL_ZERO_STRUCT(stcGpioCfg);     
    ///<TX
	  stcGpioCfg.enPu = GpioPuEnable;
    stcGpioCfg.enDir =  GpioDirOut;
    Gpio_Init(GpioPortB,GpioPin12,&stcGpioCfg);
    Gpio_SetAfMode(GpioPortB,GpioPin12,GpioAf3); //PB12 LPUART0_TX
    //<RX
	  stcGpioCfg.enPu = GpioPuEnable;
    stcGpioCfg.enDir =  GpioDirIn;
    Gpio_Init(GpioPortB,GpioPin11,&stcGpioCfg);
    Gpio_SetAfMode(GpioPortB,GpioPin11,GpioAf3); //PB11 LPUART0_RX
*/	



	
	
//Uart0	
    DDL_ZERO_STRUCT(stcGpioCfg);     
    ///<TX
	  stcGpioCfg.enPu = GpioPuEnable;
    stcGpioCfg.enDir =  GpioDirOut;
    Gpio_Init(GpioPortB,GpioPin6,&stcGpioCfg);
    Gpio_SetAfMode(GpioPortB,GpioPin6,GpioAf2); //PB6 UART0_TX
    //<RX
	  stcGpioCfg.enPu = GpioPuEnable;
    stcGpioCfg.enDir =  GpioDirIn;
    Gpio_Init(GpioPortB,GpioPin7,&stcGpioCfg);
    Gpio_SetAfMode(GpioPortB,GpioPin7,GpioAf2); //PB7 UART0_RX
	
	
}
//===================================================================================
void gpio_uart_Deinit(void)
{
    Gpio_SetAfMode(GpioPortA, GpioPin0,GpioAf0);//
    Gpio_SetAfMode(GpioPortA, GpioPin1,GpioAf0);//
	  M0P_GPIO->PAPU_f.PA00=0;
	  M0P_GPIO->PAPU_f.PA01=0;
	  M0P_GPIO->PAOUT_f.PA00=0; 
	  M0P_GPIO->PAOUT_f.PA01=0; 
}
//===================================================================================

void gpio_adc_init(void)
{
	//Gpio_SetAnalogMode(GpioPortA,GpioPin5);
	//Gpio_SetAnalogMode(GpioPortA,GpioPin7);
	Gpio_SetAnalogMode(GpioPortA,GpioPin2);
}


void gpio_pcnt_init(void)
{	
	  stc_gpio_cfg_t stcGpioCfg;

    DDL_ZERO_STRUCT(stcGpioCfg);     
	  stcGpioCfg.enPu 	= 	GpioPuEnable;
    stcGpioCfg.enDir 	=  	GpioDirIn;
		stcGpioCfg.enDrv 	=		GpioDrvH;
		stcGpioCfg.enCtrlMode=GpioAHB ;
    Gpio_Init(GpioPortA,GpioPin12,&stcGpioCfg);
    Gpio_SetAfMode(GpioPortA,GpioPin12,GpioAf7); //PA12，复用7为PCNT的S0输入
}


void gpio_tim0_init(void)
{	
	  stc_gpio_cfg_t tempGpioCfg;

    DDL_ZERO_STRUCT(tempGpioCfg); 
	
    tempGpioCfg.enDir = GpioDirOut;
    tempGpioCfg.enDrv = GpioDrvH; 
    tempGpioCfg.enPu = GpioPuDisable;
		tempGpioCfg.enPd = GpioPdDisable;
    tempGpioCfg.enOD = GpioOdDisable;	
    tempGpioCfg.enCtrlMode = GpioAHB;	
	
	
    Gpio_Init(GpioPortA,GpioPin15,&tempGpioCfg);
    Gpio_SetAfMode(GpioPortA,GpioPin15,GpioAf5);//PA15，复用5为TIM0的cha
	

	  Gpio_Init(GpioPortB,GpioPin3,&tempGpioCfg);
    Gpio_SetAfMode(GpioPortB,GpioPin3,GpioAf2); //PB3，复用2为TIM0的chb
	
}



/**
 ******************************************************************************
 ** \brief  初始化外部GPIO引脚
 **
 ** \return 无
 ******************************************************************************/
void gpio_LCD_init(void)
{
    Gpio_SetAnalogMode(GpioPortA, GpioPin9);  //COM0
    Gpio_SetAnalogMode(GpioPortA, GpioPin10); //COM1
    Gpio_SetAnalogMode(GpioPortA, GpioPin11); //COM2
    Gpio_SetAnalogMode(GpioPortA, GpioPin12); //COM3   

    Gpio_SetAnalogMode(GpioPortA, GpioPin8);  //SEG0
    Gpio_SetAnalogMode(GpioPortC, GpioPin9);  //SEG1
    Gpio_SetAnalogMode(GpioPortC, GpioPin8);  //SEG2
    Gpio_SetAnalogMode(GpioPortC, GpioPin7);  //SEG3
    Gpio_SetAnalogMode(GpioPortC, GpioPin6);  //SEG4
    Gpio_SetAnalogMode(GpioPortB, GpioPin15); //SEG5
    Gpio_SetAnalogMode(GpioPortB, GpioPin14); //SEG6
    Gpio_SetAnalogMode(GpioPortB, GpioPin13); //SEG7
    Gpio_SetAnalogMode(GpioPortB, GpioPin3);  //VLCDH
    Gpio_SetAnalogMode(GpioPortB, GpioPin4);  //VLCD3
    Gpio_SetAnalogMode(GpioPortB, GpioPin5);  //VLCD2
    Gpio_SetAnalogMode(GpioPortB, GpioPin6);  //VLCD1
    
}


void gpio_spi0_init(void)
{
	  stc_gpio_cfg_t stcGpioCfg;

    DDL_ZERO_STRUCT(stcGpioCfg);     
    //mosi
	  stcGpioCfg.enPu = GpioPuDisable;
    stcGpioCfg.enDir =  GpioDirOut;
    Gpio_Init(GpioPortB,GpioPin5,&stcGpioCfg);
    Gpio_SetAfMode(GpioPortB,GpioPin5,GpioAf1); //spi0_mosi
	
    //miso
	  stcGpioCfg.enPu = GpioPuDisable;
    stcGpioCfg.enDir =  GpioDirIn;
    Gpio_Init(GpioPortA,GpioPin6,&stcGpioCfg);
    Gpio_SetAfMode(GpioPortA,GpioPin6,GpioAf1); //spi0_miso
	
    //cs
	  stcGpioCfg.enPu = GpioPuEnable;
    stcGpioCfg.enDir =  GpioDirOut;
    Gpio_Init(GpioPortA,GpioPin4,&stcGpioCfg);
    Gpio_SetAfMode(GpioPortA,GpioPin4,GpioAf1); //spi0_cs
	
    //clk
	  stcGpioCfg.enPu = GpioPuDisable;
    stcGpioCfg.enDir =  GpioDirOut;
    Gpio_Init(GpioPortA,GpioPin5,&stcGpioCfg);
    Gpio_SetAfMode(GpioPortA,GpioPin5,GpioAf1); //spi0_clk
}



void gpio_oled_init(void)
{
	  stc_gpio_cfg_t stcGpioCfg;

    DDL_ZERO_STRUCT(stcGpioCfg);     
    //mosi-sda
	  stcGpioCfg.enPu = GpioPuDisable;
    stcGpioCfg.enDir =  GpioDirOut;
    Gpio_Init(GpioPortB,GpioPin5,&stcGpioCfg);
	
    //_rst
	  stcGpioCfg.enPu = GpioPuDisable;
    stcGpioCfg.enDir =  GpioDirOut;
    Gpio_Init(GpioPortA,GpioPin6,&stcGpioCfg);
	
    //dc
	  stcGpioCfg.enPu = GpioPuEnable;
    stcGpioCfg.enDir =  GpioDirOut;
    Gpio_Init(GpioPortA,GpioPin4,&stcGpioCfg);
	
    //clk
	  stcGpioCfg.enPu = GpioPuDisable;
    stcGpioCfg.enDir =  GpioDirOut;
    Gpio_Init(GpioPortA,GpioPin5,&stcGpioCfg);
}






void gpio_modbus_init(void)
{
	  stc_gpio_cfg_t tempGpioCfg;
        
    tempGpioCfg.enDir = GpioDirOut;
    tempGpioCfg.enDrv = GpioDrvH; 
    tempGpioCfg.enPu = GpioPuDisable;
		tempGpioCfg.enPd = GpioPdEnable;
    tempGpioCfg.enOD = GpioOdDisable;
    tempGpioCfg.enCtrlMode = GpioAHB;
    Gpio_Init(GpioPortB, GpioPin5, &tempGpioCfg); //PB5作为modbus的方向控制脚
}








void my_gpio_init(void)
{	
	
  Sysctrl_SetPeripheralGate(SysctrlPeripheralGpio, TRUE);	
	gpio_io_init();
//	gpio_uart_init();
//	gpio_adc_init();
	//gpio_pcnt_init();
	//gpio_tim0_init();
	//gpio_LCD_init();
	//gpio_oled_init();
	//gpio_spi0_init();
	//gpio_modbus_init();
}

//===================================================================================
