#include "mylvd.h"
#include "Myinit.h"

unsigned int PWR_ON_times=0;//掉电次数记录

//LVD 中断服务函数
void Lvd_IRQHandler(void)
{
	//rt_interrupt_enter();	
	
	PWR_ON_times++;	
	my_eeprom_write	(	&my_eeprom_handler,10,PWR_ON_times);

		
  Lvd_ClearIrq();
//rt_interrupt_leave();
}
void my_lvd_init(void)
{
	
	
    stc_lvd_cfg_t stcLvdCfg;

    DDL_ZERO_STRUCT(stcLvdCfg);     //变量清0
	
	
	/****插入掉电保持变量上电内容更新******/	
		PWR_ON_times=my_eeprom_read	(	&my_eeprom_handler,10);//掉电保持变量更新,所以其必须在flash 和eeprom初始化之后
	/****插入掉电保持变量上电内容更新******/	

	
	
    Sysctrl_SetPeripheralGate(SysctrlPeripheralVcLvd, TRUE);    //开LVD时钟

    stcLvdCfg.enAct        = LvdActMskInt;            // 	配置触发产生中断
    stcLvdCfg.enInputSrc   = LvdInputSrcMskVCC;       // 	配置LVD输入源
    stcLvdCfg.enThreshold  = LvdMskTH3_0V;            // 	配置LVD基准电压
    stcLvdCfg.enFilter     = LvdFilterMskEnable;      // 	滤波使能
    stcLvdCfg.enFilterTime = LvdFilterMsk28_8ms;        	//	滤波时间设置
    stcLvdCfg.enIrqType    = LvdIrqMskRise;           // 	中断触发类型
    Lvd_Init(&stcLvdCfg);
    
    //中断开启
    Lvd_EnableIrq();
    Lvd_ClearIrq();
    EnableNvic(LVD_IRQn, IrqLevel0, TRUE);              ///< NVIC 中断使能
    
    //LVD 模块使能
    Lvd_Enable();
}


