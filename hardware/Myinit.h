/* 
******************************************************************************************************

******************************************************************************************************
*/
#ifndef		_MYINIT_H_
#define		_MYINIT_H_
//=============================================#include===============================================
#include <string.h>

#include	"library.h"
#include  "myGpio.h"
#include 	"myTimer.h"
#include 	"myADC.h"
#include	"myUart.h"
#include 	"myRtc.h"
#include 	"myBT.h"
#include 	"myWdt.h"
#include 	"myCRC.h"
#include 	"myFlash.h"
#include  "myPcnt.h"
#include  "myLptimer.h"
#include 	"myLvd.h"
#include 	"my_config.h"
#include 	"my_debug.h"
#include 	"myLCD.h"
#include	"mySPI.h"
#include 	"myESP8266.h"



#define MY_DEBUG  //��������

#ifndef ENABLE 
typedef enum{
		DISABLE=0,
		ENABLE=1, 
} FunctionalState;

#endif


int my_init(void);

#endif
