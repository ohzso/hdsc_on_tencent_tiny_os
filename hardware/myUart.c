


#include	"myuart.h"

char str_buf[TEMP_STR_BUF];		//存放无用返回字符串

static unsigned int BufferNext(unsigned int buffer_length,unsigned int i);//获取下一个节点

 unsigned char ReadBufferLpuart1[ReadBufLen_Lpuart1];//分配缓冲区，默认256字节（一个奇怪的问题，数组有几个固定元素写入数据总是出错，但是申请大一点，这些出错点不用，就没问题,怀疑keil编译器或者华大mcu有问题）
 unsigned int Last_lpuart1=0,Empty_lpuart1=0;//未读字符位置，最近空位
 char str_buffer_lpuart1[Lpuart1_Line_Max];//lpuart1的行缓存

void my_lp_uart1_init(void);


static unsigned int BufferNext(unsigned int buffer_length,unsigned int i)
{

	unsigned int n;
	if(i  < buffer_length-1)
	 n=i+1;
	if(i == buffer_length-1)
		n=0;
	return n;
}


void my_uart_init(void)
{  
	my_lp_uart1_init();	
}


/*---------------------------------------------lp uart 1------start------------------------*/
void my_lp_uart1_init(void)
{

    stc_lpuart_cfg_t  stcCfg;

    DDL_ZERO_STRUCT(stcCfg);                        ///< 结构体变量初始值置零

    ///<外设模块时钟使能
    Sysctrl_SetPeripheralGate(SysctrlPeripheralLpUart1,TRUE);

    ///<LPUART 初始化
    stcCfg.enStopBit = LPUart1bit;                   ///<1停止位
    stcCfg.stcBaud.enSclkSel = LPUartMskPclk;        ///<传输时钟源
    stcCfg.stcBaud.u32Sclk = Sysctrl_GetPClkFreq();                 ///<
    stcCfg.stcBaud.enSclkDiv = LPUartMsk4Or8Div;     ///<采样分频
    stcCfg.stcBaud.u32Baud = 115200;                   ///<波特率
		//stcCfg.enMmdorCk=LPUartOdd;//LPUartEven;				//偶校验
    stcCfg.enRunMode = LPUartMskMode1;               ///<工作模式
    LPUart_Init(M0P_LPUART1, &stcCfg);

    ///<LPUART 中断使能
    LPUart_ClrStatus(M0P_LPUART1,LPUartRC);         ///<清接收中断请求
    LPUart_ClrStatus(M0P_LPUART1,LPUartTC);         ///<清发送中断请求
    LPUart_EnableIrq(M0P_LPUART1,LPUartRxIrq);     ///<禁止接收中断
   // LPUart_EnableIrq(M0P_LPUART1,LPUartTxIrq);      ///<使能发送中断

    EnableNvic(LPUART1_IRQn,IrqLevel1,TRUE);        ///<系统中断使能

}	
	
	


void LpUart1_IRQHandler(void)
{
	
//	rt_uint32_t level;

//	 rt_interrupt_enter();
	 if(LPUart_GetStatus(M0P_LPUART1, LPUartRC))       ///接收数据中断
    {
        LPUart_ClrStatus(M0P_LPUART1, LPUartRC);      ///<清接收中断请求			

        ReadBufferLpuart1[Empty_lpuart1]= LPUart_ReceiveData(M0P_LPUART1);   ///读取数据,存入buffer
				Empty_lpuart1= BufferNext(ReadBufLen_Lpuart1,Empty_lpuart1);	

				if(Empty_lpuart1==Last_lpuart1)							//更新最新字符游标位置，尽可能多保存已有数据，防止首尾覆盖
				{
//					level = rt_hw_interrupt_disable();//临时关闭中断，防止数据同时修改			
					Last_lpuart1=BufferNext(ReadBufLen_Lpuart1,Last_lpuart1);
//					rt_hw_interrupt_enable(level);	//打开中断，可以继续使用。
				}
				
			
    }	
//    rt_interrupt_leave();
}

void my_lp_uart1_send( const char data)
{ 
		M0P_LPUART1->ICR_f.TCCF = 0;
    M0P_LPUART1->SBUF_f.DATA=data;
		while(0==M0P_LPUART1->ISR_f.TC)
		{
		}
}


unsigned int Luart1_getchar(void)			//用户使用这个函数，获取缓冲中的字符，可多线程使用。超过缓冲未处理，会造成溢出循环覆盖。
{
//	rt_uint32_t level;
	unsigned int data;
	
	data=~0;
	
//	level = rt_hw_interrupt_disable();//临时关闭中断，防止数据同时修改	
	if(Last_lpuart1!=Empty_lpuart1)
	{			
		data=ReadBufferLpuart1[Last_lpuart1];	
		Last_lpuart1=BufferNext(ReadBufLen_Lpuart1,Last_lpuart1);		
	}
//	rt_hw_interrupt_enable(level);	//打开中断，可以继续使用。
	
	return data;
}


void Luart1_flush(void)		//清空缓冲区；
{
//	rt_uint32_t level;
//	
//	level = rt_hw_interrupt_disable();//临时关闭中断，防止数据同时修改	
	
	Last_lpuart1=Empty_lpuart1=0;
	
//	rt_hw_interrupt_enable(level);	//打开中断，可以继续使用。
}


char*  Luart1_get_line_none_block(unsigned int str_length_max)//接收一个完整的行，不阻塞，无效直接返回0(会收录\r \n）
{
	static unsigned int i=0,line_start_flag=FALSE;
	static int data=0;
	unsigned int time_acc=0;
	
	for(time_acc=0;time_acc<str_length_max;time_acc+=1)
	{
	
			data=Luart1_getchar();
			
			if( (line_start_flag== FALSE) && (data != ~0) &&	(data != '\r') && (data != '\n')&& (data != ' ') )
				line_start_flag=TRUE;//标记开始录入一行
				
			if((line_start_flag == TRUE) && data !=  ~0)
			{
				str_buffer_lpuart1[i]=(data&0xff);		//存入行缓存	
				
				i++;

				if(data == '\r' || data == '\n' || (i==str_length_max-1))
				{
					str_buffer_lpuart1[i]='\0';
					
					line_start_flag=FALSE;						//清除开始标记
					i=0;
					
					return str_buffer_lpuart1;
				}	
				
			}
			
//			rt_thread_mdelay(1);

	}	
	return 0;

}


char*  Luart1_get_line(unsigned int str_length_max)
{
	unsigned int i=0;
	int data=0;
	
	//等待头尾无效字符	
	do{
//		rt_thread_mdelay(1);	
		data=Luart1_getchar();
	}while((data == ~0) ||	(data == '\r') || (data == '\n'));
	

	do{	
			str_buffer_lpuart1[i]=(data&0xff);		//存入行缓存	
		
			while((data=Luart1_getchar()) ==  ~0)
//				rt_thread_mdelay(1);
			
			i++;
			
			if(data == '\r' || data == '\n')
			{
				str_buffer_lpuart1[i]='\0';
				return str_buffer_lpuart1;
			}				
			
	}while(i<str_length_max-1);
	
	return 0;

}



/*---------------------------------------------lp uart 1------end ------------------------*/
	


/*-----------------------------------------file end-------------------------------------*/
	
	
	
	