#include "mybt.h"




/*******************************************************************************
 * TIM0中断服务函数
 ******************************************************************************/
void Tim0_IRQHandler(void)
{
  //Timer0 模式0 溢出中断
  if(TRUE == Bt_GetIntFlag(TIM0, BtUevIrq))
  {
		
    Bt_ClearIntFlag(TIM0,BtUevIrq);                 ///< 中断标志清零
    Bt_M0_Stop(TIM0);                                ///< 停止定时器计时
  }
}



//Timer0配置初始化
void my_timer0_mode0_cfg(void)
{
  stc_bt_mode0_cfg_t     stcBtBaseCfg;
  DDL_ZERO_STRUCT(stcBtBaseCfg);
    
  Sysctrl_SetPeripheralGate(SysctrlPeripheralBaseTim, TRUE); //Base Timer外设时钟使能
    
  stcBtBaseCfg.enWorkMode = BtWorkMode0;                  //定时器模式0
  stcBtBaseCfg.enCT       = BtTimer;                      //定时器功能，计数时钟为内部PCLK
  stcBtBaseCfg.enPRS      = BtPCLKDiv32;                 //PCLK/256
  stcBtBaseCfg.enCntMode  = Bt16bitArrMode;               //自动重载16位计数器/定时器
  stcBtBaseCfg.bEnTog     = FALSE;
  stcBtBaseCfg.bEnGate    = FALSE;
  stcBtBaseCfg.enGateP    = BtGatePositive;
  Bt_Mode0_Init(TIM0, &stcBtBaseCfg);                     //TIM0 的模式0功能初始化
    
  Bt_ClearIntFlag(TIM0,BtUevIrq);                         //清中断标志   
  Bt_Mode0_EnableIrq(TIM0);                               //使能TIM0中断(模式0时只有一个中断)
  EnableNvic(TIM0_IRQn, IrqLevel1, TRUE);                 //TIM0中断使能
}

void my_timer0_start(uint16_t u16ArrValue)//  根据波特率来设定时间  //  u16ArrValue=PCLK/BtPCLKDiv32*time(s); 
{
	 Bt_M0_Stop(TIM0);    
   Bt_M0_Cnt16Set(TIM0,0x10000-u16ArrValue);               //设置CNT计数初值，计到0xFFFF溢出中断
   Bt_M0_Run(TIM0);                                        //TIM0 运行。
}







void my_timer0_mode2_cfg(void)
{
  stc_bt_mode23_cfg_t     				temp_mode23_cfg;
	stc_bt_m23_compare_cfg_t  			temp_cmp_cfg;
	stc_bt_m23_master_slave_cfg_t		temp_ms_cfg;
	
	
  DDL_ZERO_STRUCT(temp_mode23_cfg);
  DDL_ZERO_STRUCT(temp_cmp_cfg);
  DDL_ZERO_STRUCT(temp_ms_cfg);
	
	Sysctrl_SetPeripheralGate(SysctrlPeripheralBaseTim, TRUE); //Base Timer外设时钟使能
	
	//pwm工作方式设置
	temp_mode23_cfg.bOneShot			= FALSE;//连续模式-关闭单次模式
	temp_mode23_cfg.bURSSel				=	FALSE;//更新源选择-参照文档
	temp_mode23_cfg.enCntDir			=	BtCntUp;//向上计数
	temp_mode23_cfg.enCT					=	BtTimer;//定时器模式（定时器模式/计数模式）
	temp_mode23_cfg.enPRS					=	BtPCLKDiv1;//预分频
	temp_mode23_cfg.enPWM2sSel		=	BtSinglePointCmp;//单点比较模式
	temp_mode23_cfg.enPWMTypeSel	=	BtIndependentPWM;//独立pwm模式
	temp_mode23_cfg.enWorkMode		=	BtWorkMode2;//锯齿波模式
	Bt_Mode23_Init(TIM0, &temp_mode23_cfg);
		

	
	Bt_M23_ARRSet(TIM0, 1000, TRUE); //重载值，影响PWM周期

  Bt_M23_Cnt16Set(TIM0, 0);//设置当前值
	Bt_M23_CCR_Set(TIM0, BtCCR0A,500);//50%占空比	
	Bt_M23_CCR_Set(TIM0, BtCCR0B,250);//25%占空比
	
	

	//输出通道设置
	temp_cmp_cfg.bCh0ACmpBufEn			=	TRUE;
	temp_cmp_cfg.bCH0BCmpBufEn			=	TRUE;
	temp_cmp_cfg.enCh0ACmpCap				=	BtCHxCmpMode;//通道0设置为比较模式
	temp_cmp_cfg.enCH0ACmpCtrl			=	BtPWMMode1;//pwm1
	temp_cmp_cfg.enCh0ACmpIntSel		=	BtCmpIntNone;//无比较中断
	temp_cmp_cfg.enCH0APolarity			=	BtPortPositive;//ch0a 输出相位：正常
	temp_cmp_cfg.enCh0BCmpCap				=	BtCHxCmpMode;
	temp_cmp_cfg.enCH0BCmpCtrl			=	BtPWMMode1;
	temp_cmp_cfg.enCH0BCmpIntSel		=	BtCmpIntNone;
	temp_cmp_cfg.enCH0BPolarity			=	BtPortPositive;
	Bt_M23_PortOutput_Cfg(TIM0,&temp_cmp_cfg);
	
//主从相关的模式设置
	temp_ms_cfg.enMasterSlaveSel	=	BtMasterMode;
	temp_ms_cfg.enMasterSrc				=	BtMasterCTEN;
	temp_ms_cfg.enSlaveModeSel		=	BtSlaveResetTIM;
//temp_ms_cfg.enTsSel						=	;
	Bt_M23_MasterSlave_Set(TIM0, &temp_ms_cfg);
	
	
 //输出使能
	Bt_M23_EnPWM_Output(TIM0, TRUE, FALSE);

//启动运行
	Bt_M23_Run(TIM0);

}


/*
		PWM run test in main thread	
		if(i>1000)
				i=0;
		i+=10;
		Bt_M23_CCR_Set(TIM0, BtCCR0B,i);	
*/


void my_timer_init(void)
{
	my_timer0_mode0_cfg();
	my_timer0_mode2_cfg();//timer0的pwmm模式设置
}






