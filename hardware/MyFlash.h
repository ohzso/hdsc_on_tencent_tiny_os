

/*************************************
移植时，需要注意按规定填写falsh相关信息
华大单片机规定，flash写函数地址要小于32768，需要注意，本次测试未处理这个问题，后期如果实际需要使用，一定要加上
by hzs
2021年1月29日
**************************************/




#ifndef __MY_FLASH__
#define __MY_FLASH__

#include "hc32l13x.h"
#include "flash.h"


/**************************用户根据芯片定义*************start***********************/

#define PAG_SIZE          			512           //基本flash页面的大小，华大hc32l136 是512字节
#define END_ADDR								0xFFFF				//flash区域的最终地址，华大hc32l136 是0xFFFF
#define TRY_TIMS								100						//写入失败，重新尝试次数，根据flash性能决定，华大hc临时设置为100次，超过放弃写入

/**************************用户根据芯片定义*************stop************************/


#define BASE_ADDR_B								(END_ADDR+1-PAG_SIZE)		//flash模拟eeprom,B区域的起始地址
#define BASE_ADDR_A								(BASE_ADDR_B-PAG_SIZE)	//flash模拟eeprom,A区域的起始地址
#define MAX_OF_TAGS								(PAG_SIZE/sizeof(FLASH_ENTRY_t)/2)//总模拟数据量（所有存取使用的都是虚拟地址：tag)每个模拟变量体积为8字节，建议所有变量总逻辑体积保持在大约50%PAGE_SIZE的容量




//发现用enum,变量总是整型的，这样不太好，有时候必须要char型的。
#define	PAGE_WRITE_OVER  ((unsigned char)(0x01))       	//页面切换OK标记
#define	PAGE_WRITE_NONE  ((unsigned char)(~0))					//页面未使用或未切换完毕标记
#define	PAGE_WRITE_MIDD  ((unsigned char)(0x04))				//标记判断阈值
	

#define EEPROM_UINT				0x01
#define EEPROM_UCHR				0x02
#define EEPROM_INT				0x03
#define EEPROM_CHAR				0x04
#define EEPROM_FLOT				0x05
#define EEPROM_RESV				(~0x00)


/*
区域状态：1111:empty  1110:正在写入  1100:valid   1000:erased   0000:bad	
此处重复写入同一个flash区域，所以必须只能利用固定的几种不可逆状态转换
*/
#define ENTRY_EMPTY 		(unsigned char)(~0x00)

#define ENTRY_PGING 		(unsigned char)(~0x01)
#define ENTRY_VALID 		(unsigned char)(~0x03)
#define ENTRY_ERASE 		(unsigned char)(~0x07)
#define ENTRY_BAD 			(unsigned char)(0)


#define ERROR_TAG_NOT_FOUND		1				//错误码：未找到TAG




//===================================================================================
enum PAGE_TAG
{
	FLASH_PAGE_A=1,
	FLASH_PAGE_B=2,
};



enum PAGE_STAT
{
	
	PAGE_FULL_LOG=1,//当前页逻辑地址用完，无法装入新变量
	PAGE_FULL_PHY=2,//当前页逻辑地址还有剩余，但是实际空间用完，需要换页操作
	PAGE_FULL_NOT=3,//当前页有剩余逻辑地址和实际空间，可以正常写入。
};

typedef struct 
{
	unsigned int  base_address_A;
	unsigned int  base_address_B;
	unsigned int  page_size;
	unsigned int  max_number_of_tags;
	unsigned int  current_empty;//当前空地址相对偏移量（调用flash_eeprom_empty_check会更新）
	unsigned char error;//
	unsigned char format;//格式化标记： 0x0f :Flash 检测到未格式化，已执行格式化
	unsigned int  active_end_address;//活动页结束地址
	unsigned int  active_top_address;//活动页开始地址
} eeprom_handler_t;
//后期可加入当前使用页、最新空位置的地址、最近几次使用变量与物理地址对照表等信息，帮助更快检索数据


typedef struct 
{
	unsigned int  base_address;
	unsigned int  page_size;
	unsigned int  number_of_tags;
} eeprom_cfg_t;





typedef struct
{	
	unsigned char type;	//数据类型：1：uint，2：uchar 3:int 4:char 5：reserved
	unsigned char flag;	//区域状态：111:empty   101:valid   100:erased   000:bad	此处重复写入同一个flash区域，所以必须只能利用固定的几种不可逆状态转换
	unsigned char tag;	//tag值（每个数据都有一个tag，类似于其它文件里的“虚拟地址”的概念，数据通过虚拟地址tag来获取）
	unsigned char crc;	//CRC校验值，暂时未用
	unsigned int 	data;	//实际数据存放，32位	
} FLASH_ENTRY_t;








typedef struct
{ 													//注意此结构体最好不要改变已有变量的内存分布，会影响格式化函数。(已变更写法，不会有影响）
	unsigned int 	conuter;		//整页面擦写翻转次数记录，32位  0xffffffff 表示未使用，空页面,两个AB页面，此值小的表示正在使用中，
	unsigned char flag_using;	//切换成功标记：每次切换，最后一个写入，上电检查，如果页面优先级高（counter小）,写入标记却为oxff，则说明切换中掉电失败
	unsigned char reserved02;
	unsigned char reserved03;
	unsigned char reserved04;	
} FLASH_STATUS_t;
/*
使用中页面判别方法：
1：读取反转次数（减计数），小则说明正在使用中
2：读取flag_using，初始值是0xff，页面正常反转，则被改写为0x0f，初始化读取此值，如果小于0xf0，则说明页面正常，未受掉电影响
3:一般切换页面前，擦除备份页面，然后搬运数据至备份页面，数据搬运完毕后，
	最后依次写入反转次数counter-1，flag_using处写入0x01。
	每次上电时，通过比较正在使用页面的flag_using与0xf0的大小，确定意外断电前是否成功切换完所有数据，若未写完，则需要重写（其实仅用来保证反转计数是否正确）。
*/


/**************************用户使用函数*************start************************/
int 					my_flash_init		(	void										 																  );//OKflash硬件参数初始化,必须首先执行，然后调用formate 和init，完成所有准备工作
int 					my_eeprom_format(	const eeprom_cfg_t * ,		eeprom_handler_t *					 		);//OK强制格式化flash区域（出厂使用一次，后期不可使用）
int 					my_eeprom_init	( const eeprom_cfg_t *    , eeprom_handler_t *	  				  );//OK根据cfg的参数，对handler内容进行完善，并调用check函数，决定是否需要格式化（先实现掉电切换错误纠正，不格式化）
unsigned int	my_eeprom_read	(				eeprom_handler_t *, unsigned char tag							  );//OK根据tag,找出flash中对应的data
int 					my_eeprom_write	( 			eeprom_handler_t *, unsigned char tag, unsigned int	);//OK
int 					my_eeprom_delet	( 			eeprom_handler_t *, unsigned char tag							  );//OK
//用户需要自己定义一个cfg变量，并手动填写，然后再定义一个handler变量，作为控制参数集

/**************************用户使用函数*************stop*************************/


//===================================================================================

int 						flash_eeprom_entry_write	(eeprom_handler_t *,unsigned int data,unsigned char type);//OK单页内写入一个条目，无页面切换功能
int 						flash_eeprom_pwron_check	(const eeprom_handler_t *		);				//OK检查并更正falsh的两个page切换过程中掉电引发的错误，每次上电初始化中使用一次
int 						flash_eeprom_page_switch	(const eeprom_handler_t *,enum PAGE_TAG to);//OK切换并压缩数据至备份页
unsigned int 		my_flash_read							(unsigned int addr			 		);				//OK库函数缺少读取函数，此处根据硬件地址，读取一个uint
enum PAGE_TAG		current_page							(const eeprom_handler_t *		);				//OK找出并返回当前使用中的页（A或B）
int 						flash_eeprom_update_active_address(	eeprom_handler_t *handler	);//OK更新最新的活动数据区起止地址。


extern eeprom_handler_t my_eeprom_handler;



/*
记：
可以考虑上电init时，调用pwron_check函数内，检查
flash基本结构（反转次数记录与写完毕记录数据是否
正常，来判断flash是否已经被格式化过，否则提示用
户进行格式化，或自动调用fromat函数进行flash格式化。
*/




#endif
//===================================================================================
