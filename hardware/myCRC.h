#ifndef __MYCRC_H__
#define __MYCRC_H__

#include "ddl.h"

void CalcCrc(uint8_t crcbuf );
uint8_t Crc_check(uint8_t rx_len,uint8_t rx_crc_h,uint8_t rx_crc_l,uint8_t *pdata);
uint16_t Crc_cal(uint8_t *pdata,uint8_t len);



#endif
