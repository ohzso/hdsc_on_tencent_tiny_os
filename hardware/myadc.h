#ifndef __MYADC_H_
#define __MYADC_H_

#include "adc.h"
#include "bgr.h"

int my_adc_init(void);
unsigned int GetADC_value(unsigned int channel);//获取ADC的值，channel=0,PA2
unsigned int ADC_GET_FLAG(void);//指示ADC是否准备好
unsigned int ADC_START(void);//重启ADC转换

#endif
