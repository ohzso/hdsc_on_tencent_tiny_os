

#ifndef __LIBRARY_H
#define __LIBRARY_H

#include "ddl.h"   
#include "gpio.h" 
#include "hc32l13x.h" 
#include "bt.h"
#include "interrupts_hc32l13x.h" 
#include "lpm.h" 
#include "lptim.h"
#include "i2c.h"
#include "math.h"
#include "uart.h"	
#include "pcnt.h"	
#include "lpuart.h"	
#include "crc.h"	
#include  "wdt.h"
#include  "bgr.h"
#include  "lvd.h"
#endif
