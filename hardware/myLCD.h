#ifndef		_MYLCD_H_
#define		_MYLCD_H_
//=============================================#include===============================================
#include 	"myInit.h"
#include 	"lcd.h"
#include	"library.h"
//==============================================外部引用==============================================
#define LCD_RAM_BASE 	0x40005C00
#define LCD_RAM_0			*((unsigned char *)(LCD_RAM_BASE+0x40))
#define LCD_RAM_1			*((unsigned char *)(LCD_RAM_BASE+0x44))
#define LCD_RAM_2			*((unsigned char *)(LCD_RAM_BASE+0x48))
#define LCD_RAM_3			*((unsigned char *)(LCD_RAM_BASE+0x4C))

//============================================= 函数声明 ===============================================
void my_lcd_init(void);
void my_lcd_ram_write(unsigned int RAM_DATA);
extern const unsigned int lcd_num_table[4][10];

void my_lcd_num(unsigned int number);//在LCD上显示一个十进制整数（范围0-9999）
void my_lcd_num_v2(unsigned int number);//将整型变量（0-9999）分解显示到LCD屏幕上
int my_lcd_char_disp(unsigned char lcd_pos,unsigned char char_);//将一个字符显示在LCD屏幕上（a-z)

#endif
