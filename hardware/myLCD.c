#include "myLCD.h"

void gpio_LCD(void);
void _LcdCfg_(void);
/**
 ******************************************************************************
 ** \brief  主函数
 ** 
 ** @param  无
 ** \retval 无
 **
 ******************************************************************************/ 
 
 static unsigned int my_lcd_ram;//每一个位，代表一个LCD元素（1显示，0不显示），由my_lcd_ram_write()函数映射到LCD的显示RAM.
 
void my_lcd_init(void)
{
    Sysctrl_ClkSourceEnable(SysctrlClkRCL,TRUE);            ///< 使能RCL时钟
    Sysctrl_SetRCLTrim(SysctrlRclFreq32768);                ///< 配置内部低速时钟频率为32.768kHz

    Sysctrl_SetPeripheralGate(SysctrlPeripheralLcd,TRUE);   ///< 开启LCD时钟  
    _LcdCfg_();                ///< LCD模块配置
    Lcd_ClearDisp();             ///< 清屏
}
//


/**
 ******************************************************************************
 ** \brief  配置LCD
 **
 ** \return 无
 ******************************************************************************/
void _LcdCfg_(void)
{
    stc_lcd_cfg_t LcdInitStruct;
    stc_lcd_segcom_t LcdSegCom;

    LcdSegCom.u32Seg0_31 = 0xffffff00;                              ///< 配置LCD_POEN0寄存器 开启SEG0~SEG7
    LcdSegCom.stc_seg32_51_com0_8_t.seg32_51_com0_8 = 0xffffffff;   ///< 初始化LCD_POEN1寄存器 全部关闭输出端口
    LcdSegCom.stc_seg32_51_com0_8_t.segcom_bit.Com0_3 = 0;          ///< 使能COM0~COM3
    LcdSegCom.stc_seg32_51_com0_8_t.segcom_bit.Mux = 0;             ///< Mux=0,Seg32_35=0,BSEL=1表示:选择外部电容工作模式，内部电阻断路
    LcdSegCom.stc_seg32_51_com0_8_t.segcom_bit.Seg32_35 = 0;
    Lcd_SetSegCom(&LcdSegCom);                                      ///< LCD COMSEG端口配置

    LcdInitStruct.LcdBiasSrc = LcdExtCap;                          ///< 电容分压模式，需要外部电路配合
    LcdInitStruct.LcdDuty = LcdDuty4;                              ///< 1/4duty
    LcdInitStruct.LcdBias = LcdBias3;                              ///< 1/3 BIAS
    LcdInitStruct.LcdCpClk = LcdClk2k;                             ///< 电压泵时钟频率选择2kHz
    LcdInitStruct.LcdScanClk = LcdClk128hz;                        ///< LCD扫描频率选择128Hz
		LcdInitStruct.LcdMode = LcdMode1;                              	///< 选择模式1,这样内存分布表示比较简单。
    LcdInitStruct.LcdClkSrc = LcdRCL;                              ///< LCD时钟选择RCL
    LcdInitStruct.LcdEn   = LcdEnable;                             ///< 使能LCD模块
    Lcd_Init(&LcdInitStruct);
}
//


void my_lcd_ram_write(unsigned int RAM_DATA)//将整型参数与实际LCD显示RAM映射，这样，分散的LCDram被集中映射到这个整型变量里。
{	
	LCD_RAM_0=(RAM_DATA>> 0)&0xFF;
	LCD_RAM_1=(RAM_DATA>> 8)&0xFF;
	LCD_RAM_2=(RAM_DATA>>16)&0xFF;
	LCD_RAM_3=(RAM_DATA>>24)&0xFF;
}
//


/*GDC04212 屏幕控制说明：

  -    -     -    -  
 | |  | |   | |  | |
  -    -  :  -    - 
 | |  | |   | |  | |
  -  . -  .  -  . - 

共32个元素：
my_lcd_ram的每一个bit,经过my_lcd_ram_write()映射后，对应一个元素
对应关系如下：

          LCD->my_lcd_ram bit对应表                           | 标准顺序表(普通LED顺序)
   3段        2段           1段         0段                   |
------------------------------------------------------------------------------
   01          03           05          07										|      00
 00  09      02  11       04  13      06  15									|		05    01
   08          10     26    12          14										|      06
 16  17      18  19       20  21      22  23									|		04    02
   25    24    27     28    29     30   31										|      03
------------------------------------------------------------------------------


1：段式LCD实际上被看作是一个整体，每一部分的排列规律性不强，所以想要实现类似LED控制，更困难一些。
2：这里采用my_lcd_ram这个变量，它的每一bit位代表一个LCD显示元素，此位为0，元素消失，为1，元素显示；
通过my_lcd_ram_write()函数，可将my_lcd_ram每一位对应到LCD硬件控制器相关寄存器上，实现最终的显示；
可以很容易想到，多个元素叠加（按位或操作），放到my_lcd_ram，即可同时显示多个元素。
3：并且这里，为了方便开发，仍然希望像普通标准LED数码管那样，分段地使用这个LCD屏，为此，采用了：
lcd_pos_bit_map[_MAX_SEGMENT_][_MAX_ELEMENT_]这个字典，通过它，以及stand_2_lcd_ram_bit()这个函数
可以根据普通标准数码管的“组”、“位”，找到实际my_lcd_ram的对应bit位，并更进一步地，根据标准码表对
应位的值，对my_lcd_ram对应位进行设置,这样，就可以根据标准码表，控制LCD显示。
    简单来说，可以用普通LED的码表，经过stand_2_lcd_ram_bit()函数，转化为LCD显示码，多个显示码叠加
（异或操作），放入my_lcd_ram，用my_lcd_ram_write()，即可显示在LCD屏幕上。
*/
//

#define _MAX_ELEMENT_ 7  //直观的分类，构成每个LCD显示字符的数量，一般LED段数码管有7个基本元素
#define _MAX_SEGMENT_ 4	 //直观的分类，这个LCD可以显示4个字符，所以是4


//my_lcd_ram bit到标准顺序字典，（通过此表，可以根据字符位置、字符对应bit在标准顺序中的值，查到实际它在my_lcd_ram bit中对应的bit位）
const char lcd_pos_bit_map[_MAX_SEGMENT_][_MAX_ELEMENT_]=
{
	7,15,23,31,22,6,14,
	5,13,21,29,20,4,12,
	3,11,19,27,18,2,10,
	1, 9,17,25,16,0, 8,	
};
//


/*将标准顺序字形码 映射到LCD的字形码中*/
unsigned int stand_2_lcd_ram_bit(unsigned char lcd_pos,unsigned char stand_code,const char (*lcd_pos_bit_map_)[_MAX_ELEMENT_])
{
	unsigned int temp_lcd_ram=0,i;
	
	if(lcd_pos>=_MAX_SEGMENT_)//参数超界，直接返回，不再计算
		return 0;
	
	for(i=0;i<_MAX_ELEMENT_;i++)
	{
		temp_lcd_ram |= ((stand_code &(1<<i))==0?0:1)<<( lcd_pos_bit_map_[lcd_pos][i]);		
	}
	return temp_lcd_ram;
}

//


//a-z字母-标准顺序码表（通用、不全）
const char stand_char_table[23]=
{
	0x77,/*A*/0x7C,/*b*/0x39,/*C*/
	0x5E,/*d*/0x79,/*E*/0x71,/*F*/0x76,/*H*/0x3D,/*G*/0x74,/*h*/0x58,/*c*/0x0E,/*J*/0x38,/*L*/0x54,/*n*/
	0x37,/*N*/0x5C,/*c*/0x73,/*P*/0x67,/*q*/0x50,/*r*/0x78,/*t*/0x3E,/*U*/0x40,/*-*/0x08,/*_*/0x00,/*空*/
};
//



//a-z字母-我的顺序码表（自定义，完整）
const char my_char_table[26]=
{
/*A*/
0x77,
/*b*/
0x7C,0x58,0x5E,0x7B,0x71,
/*g*/
0x6F,0x74,0x10,0x0E,0x70,
/*l*/
0x38,0x2B,0x54,0x5C,0x73,
/*q*/
0x67,0x50,0x6D,0x78,0x1C,
/*v*/
0x2A,0x1D,0x7A,0x6E,0x1B,
};
//



int my_lcd_char_disp(unsigned char lcd_pos,unsigned char char_) //my_lcd_char_display(0,'b'),在第0个位置上显示字符"b"
{
	if(char_>'z')
		return -1;
	my_lcd_ram=0;
	my_lcd_ram |= stand_2_lcd_ram_bit(lcd_pos,my_char_table[char_-'a'],lcd_pos_bit_map);
	my_lcd_ram_write(my_lcd_ram);	
	return 0;
}
//


//0-9数字-标准顺序码表
const char stand_num_table[10]=
{
	1<<0 | 1<<1 | 1<<2 | 1<<3 | 1<<4 | 1<< 5 | 0<<6,		//0
	0<<0 | 1<<1 | 1<<2 | 0<<3 | 0<<4 | 0<< 5 | 0<<6,		//1
	1<<0 | 1<<1 | 0<<2 | 1<<3 | 1<<4 | 0<< 5 | 1<<6,		//2
	1<<0 | 1<<1 | 1<<2 | 1<<3 | 0<<4 | 0<< 5 | 1<<6,		//3
	0<<0 | 1<<1 | 1<<2 | 0<<3 | 0<<4 | 1<< 5 | 1<<6,		//4
	1<<0 | 0<<1 | 1<<2 | 1<<3 | 0<<4 | 1<< 5 | 1<<6,		//5
	1<<0 | 0<<1 | 1<<2 | 1<<3 | 1<<4 | 1<< 5 | 1<<6,		//6
	1<<0 | 1<<1 | 1<<2 | 0<<3 | 0<<4 | 0<< 5 | 0<<6,		//7
	1<<0 | 1<<1 | 1<<2 | 1<<3 | 1<<4 | 1<< 5 | 1<<6,		//8
	1<<0 | 1<<1 | 1<<2 | 1<<3 | 0<<4 | 1<< 5 | 1<<6,		//9	
};



void my_lcd_num_v2(unsigned int number)//将整型变量（0-9999）分解显示到LCD屏幕上
{
	unsigned int number_new,i;
	my_lcd_ram=0;
	for(i=0;i<_MAX_SEGMENT_;i++)
	{
		number_new = number / 10;
		my_lcd_ram |= stand_2_lcd_ram_bit(i,stand_num_table[number-number_new*10],lcd_pos_bit_map);
		number=number_new ;
		if(number==0)
			break;
	}	
	my_lcd_ram_write(my_lcd_ram);	
}


/******************************************************************************
 * EOF (not truncated)
 ******************************************************************************/


