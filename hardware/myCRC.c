

#include "mycrc.h"

uint16_t     CRC ;
 void CalcCrc(uint8_t crcbuf )
{
	 
    uint8_t i,TT;
    CRC = CRC ^ crcbuf;
    for(i = 0; i < 8; i ++)
    {
        TT = CRC & 1;
        CRC = CRC  >> 1;
        CRC = CRC  & 0x7fff;
        if(TT == 1)
           CRC = CRC ^ 0xa001;
        CRC = CRC & 0xffff;
    }
}
//**************************************************************************************//
uint8_t Crc_check(uint8_t rx_len,uint8_t rx_crc_h,uint8_t rx_crc_l,uint8_t *pdata)
{
  uint8_t Crc16_ResH;
  uint8_t Crc16_ResL;

  unsigned char i;
	 CRC =0xffff;
      for (i=0;i<rx_len-2;i++) 
          CalcCrc(pdata[i]);
      Crc16_ResL  =(uint8_t)CRC&0XFF;
      Crc16_ResH  = (CRC>>8)&0XFF;
       if ((rx_crc_l==Crc16_ResH)&&(rx_crc_h ==Crc16_ResL ))
       {
         return(1);
       }
         else return(0);
}
 uint16_t Crc_cal(uint8_t *pdata,uint8_t len)
{
  uint8_t i;
       CRC =0xffff; 
      for (i=0;i<len;i++) 
          CalcCrc(pdata[i]);
    return CRC;
}

