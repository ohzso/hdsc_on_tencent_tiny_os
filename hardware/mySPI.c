#include "mySPI.h"
#include "Myinit.h"


unsigned int spi_rx_cont=0,spi_tx_cont=0;

//SPI0 中断服务函数
void Spi0_IRQHandler(void)
{
//	rt_interrupt_enter();	
	
	
	
	if(Spi_GetStatus(M0P_SPI0,SpiTxe)==TRUE)
	{
		spi_rx_cont++;	
	};
	
	
	if(Spi_GetStatus(M0P_SPI0,SpiRxne)==TRUE)
	{
		spi_tx_cont++;
	};
	
	spi_tx_cont++;
	
	Spi_ClearStatus(M0P_SPI0);
	
//	rt_interrupt_leave();
}




//SPI 配置主发送的电平
//void Spi_SetCS(M0P_SPI_TypeDef* SPIx,boolean_t bFlag);
//SPI 数据发送
//en_result_t Spi_SendData(M0P_SPI_TypeDef* SPIx, uint8_t u8Data);
//SPI 数据接收
//uint8_t Spi_ReceiveData(M0P_SPI_TypeDef* SPIx);



void my_spi_init(void)
{
	stc_spi_cfg_t temp_spi_cfg;
	
	Sysctrl_SetPeripheralGate(SysctrlPeripheralSpi0, TRUE);    //开SPI0时钟

	temp_spi_cfg.enCPHA = SpiMskCphafirst;
	temp_spi_cfg.enCPOL = SpiMskcpollow;
	temp_spi_cfg.enPclkDiv = SpiClkMskDiv128;
	temp_spi_cfg.enSpiMode = SpiMskMaster ;		
	
	Spi_FuncEnable(M0P_SPI0, SpiMskRxNeIe | SpiMskTxEIe);//启用接收、发送中断
	
	//SPI 功能使能禁止函数
	Spi_ClearStatus(M0P_SPI0);//清除中断标志
	
//	Spi_IrqEnable(M0P_SPI0);//启用spi中断	
	
	
//	EnableNvic(SPI0_IRQn, IrqLevel2, TRUE);//设置中断优先级，并开启中断处理
	
	Spi_Init( M0P_SPI0,&temp_spi_cfg);

//	Spi_Enable();
}

